<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    protected $fillable = [
        'name',
        'license_no',
        'locality',
        'city',
        'state_id',
        'country_id',
        'description',
        'primary_email',
        'secondary_email',
        'contact_no',
        'president_name',
        'address',
        'website',
        'type_of_work_id',
        'b2b_user_id',
        'created_at',
        'updated_at',
        'status'
    ];

    public $table = "organisation";


    protected $dates = ['updated_at'];

    public function scopePublished($query){
        $query->where('updated_at','<=',Carbon::now());
    }

    public function setPublishedAtAttribute($date){
        $this->attributes['updated_at'] = Carbon::createFromFormat('Y-m-d',$date);
    }

    public function user(){
        return $this->belongsTo('1');
    }
}
