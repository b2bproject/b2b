<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/','UserController@index');
Route::post('ngo/addNGO','OrganisationController@addNGO');
Route::post('ngo/{id}','OrganisationController@updateNGO');
Route::get('user/addNGO','OrganisationController@index');
Route::get('contactOrganisation','ContactController@showOrgForm');
Route::get('contactAdmin','ContactController@showAdminForm');
Route::post('contact/sendMessage', 'ContactController@handleOrgFormPost');
Route::post('contact/sendFeedbackToAdmin', 'ContactController@handleAdminFormPost');
Route::post('orgAdmin/product/addProduct','ProductController@create');
Route::get('product/addProductDetails','ProductController@index');
Route::get('admin/ngoList','AdminController@viewNGOList');
Route::get('ngo/vieworg','OrganisationController@viewOrg');
Route::post('orgAdmin/index/addNewsSubmit','OrganisationController@addNews');
Route::get('orgAdmin/index/editProject/{id}','OrganisationController@editProject');
Route::post('orgAdmin/index/projects/{id}','OrganisationController@updateProject');
Route::get('orgAdmin/index/editEvent/{id}','OrganisationController@editEvent');
Route::post('orgAdmin/index/events/{id}','OrganisationController@updateEvent');
Route::get('orgAdmin/index/editNews/{id}','OrganisationController@editNews');
Route::post('orgAdmin/index/news/{id}','OrganisationController@updateNews');
Route::get('orgAdmin/index/editProduct/{id}','ProductController@edit');
Route::post('orgAdmin/index/product/{id}','ProductController@update');
Route::get('orgAdmin/index/editDonation/{id}','OrganisationController@editDonation');
Route::post('orgAdmin/index/donation/{id}','OrganisationController@updateDonation');
Route::get('orgAdmin/index/editVolunteerProgramme/{id}','OrganisationController@editVolunteerProgramme');
Route::post('orgAdmin/index/volunteer/{id}','OrganisationController@updateVolunteerProgramme');
Route::post('orgAdmin/index/addProjectSubmit','OrganisationController@addProject');
Route::post('orgAdmin/index/addEventSubmit','OrganisationController@addEvent');
Route::post('orgAdmin/index/addDonationSchemeSubmit','OrganisationController@addDonationScheme');
Route::post('orgAdmin/index/addVolunteerProgrammeSubmit','OrganisationController@addVolunteerProgramme');
Route::get('orgAdmin/sliderImages','OrganisationController@imagesVIew');
Route::get('orgAdmin/index/{id}','OrganisationController@home');
Route::post('orgAdmin/orgIndexButton','OrganisationController@routeOrgAdmin');
Route::post('orgAdmin/index/addImagesSubmit','OrganisationController@addImages');
Route::post('orgAdmin/index/editImagesSubmit','OrganisationController@editImages');
Route::get('orgAdmin/logo','OrganisationController@viewLogo');
Route::post('orgAdmin/changeLogo','OrganisationController@addLogo');
Route::post('orgAdmin/index/projects/{id}','OrganisationController@updateProject');
Route::get('orgAdmin/statistics','OrganisationController@viewStatistics');
Route::post('user/register','UserController@register');
Route::post('user/login','UserController@login');
Route::get('user/logout','UserController@logout');
Route::get('user/myAccount','UserController@userAccount');
Route::get('microwebsite/{id}','UserController@vieworg');
Route::get('search','SearchController@userSearch');
Route::get('admin/generateTopNGOs','AdminController@generateTopNGOs');
Route::get('admin/viewAll','AdminController@viewAll');
Route::get('admin/topNGOs','AdminController@topNGOs');
Route::get('admin/featuredNGOs','AdminController@featuredNGOs');
Route::POST('admin/generateTopNGOs','AdminController@generateTopNGOs');
Route::get('admin/index','AdminController@index');
Route::post('admin/makeFeatured','AdminController@generateFeaturedNGOs');
Route::post('admin/removeFeatured','AdminController@removeFeaturedTag');
Route::get('orgAdmin/editngo','OrganisationController@editNGO');
Route::get('browseAll','UserController@viewTopNGOs');
Route::get('browseMore','UserController@viewAllNGOs');
Route::POST('user/edit','UserController@editUserDetails');
Route::get('about',function(){
    \Session::put('page','About Us-B2B');
    return view('user.about');
});
Route::get('terms',function(){
    \Session::put('page','Terms and Conditions-B2B');
    return view('user.termsAndConditions');
});
Route::get('privacy',function(){
    \Session::put('page','Privacy Policy-B2B');
    return view('user.privacyPolicy');
});


Route::get('microwebsite/joinvolunteering/{id}','UserController@joinvolunteering');

Route::get('microwebsite/donate/{id}','UserController@donate');

Route::get('user/log',function(){
    return view('user.login');
});

Route::get('microwebsite/buyProduct/{id}','UserController@viewBuyProduct');
Route::post('microwebsite/buyProduct/confirmPurchase','UserController@buyProduct');