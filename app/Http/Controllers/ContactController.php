<?php

namespace App\Http\Controllers;

//use Illuminate\Contracts\Validation\Validator;
use Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    // This function will show the view
    public function showOrgForm()
    {
        //return View::make('contact/contactform');
        return view('contact.contactOrg');
    }

    public function showAdminForm()
    {
        //return View::make('contact/contactform');
        return view('contact.contactAdmin');
    }

    public function handleOrgFormPost()
    {
        $input = Input::only('name', 'email', 'contact_no', 'msg');
        //print_r($input);
        $validator = Validator::make($input,
            array(
                'name' => 'required',
                'email' => 'required|email',
                'contact_no' => 'required',
                'msg' => 'required'
            )
        );
        if ($validator->fails())
        {
            return view('contact.contactOrg')->with('errors', $validator->messages());
        } else { // the validation has not failed, it has passed


            // Send the email with the contactemail view, the user input

            Mail::send('contact/contactemail', $input, function($message)
            {
                $message->from('your@meail.address', 'Your Name');

                $message->to('thadaninilesh@gmail.com');
            });
            \Session::flash('flash_message', 'Your message has been sent successfully! You\'ll get a reply soon');
            // Specify a route to go to after the message is sent to provide the user feedback
            return view('contact.contactOrg');
        }
    }


    public function handleAdminFormPost()
    {
        $input = Input::only('name', 'email', 'contact_no', 'msg');
        //print_r($input);
        $validator = Validator::make($input,
            array(
                'name' => 'required',
                'email' => 'required|email',
                'contact_no' => 'required',
                'msg' => 'required'
            )
        );
        if ($validator->fails())
        {
            return view('contact.contactAdmin')->with('errors', $validator->messages());
        } else { // the validation has not failed, it has passed

            // Send the email with the contactemail view, the user input

            Mail::send('contact/contactemail', $input, function($message)
            {
                $message->from('your@email.address', 'Your Name');

                $message->to('sajnaninisha19@gmail.com');
            });
            \Session::flash('flash_message', 'Your message has been sent successfully! You\'ll get a reply soon');
            // Specify a route to go to after the message is sent to provide the user feedback
            return view('contact.contactAdmin');
        }
    }



}
