<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{
    public function index(){
        \Session::put('page','Home-B2B Management and Lead Generation');
        $forg = \DB::table('organisation')->select('*')->where('flag', '=', 1)->get();
        $events = \DB::table('events')->select('img','org_id')->orderByRaw('RAND()')->take(12)->get();
        return view('user.index',compact('forg','events'));
    }

    public function register(Request $request){
        $rules = array(
            'name' => 'required',
            'email' => 'required|Email|Unique:b2b_user',
            'mobile' => 'required|numeric',
            'password' => 'required|min:8|AlphaNum|Confirmed',
            'password_confirmation' => 'required|AlphaNum',
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }
        else{
            $name = $request->name;
            $email = $request->email;
            $mobile = $request->mobile;
            $password = hash('sha512',hash('md5',$request->password));
            $id = \DB::table('b2b_user')->insertGetId(
                ['name' => $name, 'email' => $email,'password' => $password, 'mobile_no' => $mobile, 'status' => 0]
            );
            \Session::flash('flash_message','Congrats! Registeration successful. Use '.$email.' as your login email');
            Session:put('userSession',$email);
            return redirect()->back();
        }
    }


    public function login(Request $request)
    {
        $rules = array(
            'email' => 'required|Email',
            'password' => 'required|min:8|AlphaNum',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $email = $request->email;
            $password = hash('sha512', hash('md5', $request->password));
            //\DB::table('b2b_user')->insert()
            /*$id = \DB::table('b2b_user')->insertGetId(
                ['name' => $name, 'email' => $email,'password' => $password, 'mobile_no' => $mobile, 'status' => 0]
            );*/
            $count = \DB::table('b2b_user')->where('email', '=', $email)->where('password', '=', $password)->count();
            if ($count == 0) {
                \Session::flash('error_message', 'Invalid Username or Password! Please try again');
                return redirect()->back();
            } else {
                \Session::flash('flash_message','Login Successful');
                $status = \DB::table('b2b_user')->where('email', '=', $email)->where('password', '=', $password)->pluck('status');
                $id = \DB::table('b2b_user')->where('email', '=', $email)->where('password', '=', $password)->pluck('b2b_user_id');
                $name = \DB::table('b2b_user')->where('email', '=', $email)->where('password', '=', $password)->pluck('name');
                \Session::put('userID', $id);
                \Session::put('userSession', $email);
                \Session::put('userStatus', $status);
                \Session::put('userName', $name);
                if($id > 0){
                    if($status == 0){
                        return Redirect::to('user/myAccount');
                    }
                    else if($status == 1){
                        $org_id = \DB::table('organisation')->where('b2b_user_id','=',\Session::get('userID'))->pluck('org_id');
                        $org_name = \DB::table('organisation')->where('b2b_user_id','=',\Session::get('userID'))->pluck('name');
                        \Session::put('orgID',$org_id);
                        \Session::put('org_name',$org_name);
                        return Redirect::to('orgAdmin/index/'.base64_encode($org_id));
                    }
                    else if($status == 2){
                        return Redirect::to('admin/index');
                    }
                    else{
                        return view('errors.503');
                    }
                }
                else{
                    \Session::flash('error_message', 'Invalid Username or Password! Please try again');
                    return redirect()->back();
                }
            }
        }
    }

    public function logout(){
        \Session::forget('userSession');
        \Session::flush();
        return Redirect::to('/');
    }


    public function viewOrg($id) {
        \Session::put('page','Microwebsite-B2B Management and Lead Generation');
        $product = \DB::table('product')->where('org_id','=',$id)->get();
        $event = \DB::table('events')->where('org_id','=',$id)->get();
        $project = \DB::table('project')->where('org_id','=',$id)->get();
        $organisation = \DB::table('organisation')->select('*')->where('org_id','=',$id)->get();
        \DB::table('org_view')->insert([
            ['org_id'=>$id]
        ]);
        $volunteer = \DB::table('volunteer')->where('org_id', '=', $id)->get();
        $donation = \DB::table('donation')->where('org_id', '=', $id)->get();

        $images = \DB::table('image')->select('*')->where('org_id','=',$id)->get();
        return view('user.microwebsite',compact('product','event','project','organisation','images','volunteer','donation'));
    }


    public function joinvolunteering($id)
    {
        if (\Session::has('userSession')) {
            //return view('user.joinvolunteering');
            $user_id = \Session::get('userID');
            $org_id = \DB::table('volunteer')->where('volunteer_id', '=', $id)->pluck('org_id');
            \DB::table('volunteer_user')->insert(
                ['org_id' => $org_id, 'b2b_user_id' => $user_id, 'volunteer_id' => $id]
            );
            \Session::flash('flash_message', 'Volunteering programme joined');
            return redirect()->back();
        } else
        {
            \Session::flash('error_message', 'Login to Continue');
            return Redirect::to('user/log');
            
        }
    }

    public function donate($id){
        if (\Session::has('userSession')) {
            //return view('user.joinvolunteering');
            $user_id = \Session::get('userID');
            $org_id = \DB::table('donation')->where('donation_id', '=', $id)->pluck('org_id');
            $amount = \DB::table('donation')->where('donation_id', '=', $id)->pluck('amount');
            \DB::table('donation_user')->insert(
                ['org_id' => $org_id, 'b2b_user_id' => $user_id, 'donation_id' => $id, 'amount' => $amount]
            );
            \Session::flash('flash_message', 'Thankyou for your response.You"ll be contacted from our side for donation.');
            return redirect()->back();
        } else
        {
            \Session::flash('error_message', 'Login to Continue');
            return Redirect::to('user/log');

        }
    }

    public function userAccount(){
        if(!\Session::has('userSession')){
            return view('errors.503');
        }
        \Session::put('page','My Account-B2B');
        $donation = \DB::table('donation_user')->join('donation','donation.donation_id','=','donation_user.donation_id')->where('b2b_user_id','=',\Session::get('userID'));
        $volunteer = \DB::table('volunteer_user')->join('volunteer','volunteer.volunteer_id','=','volunteer_user.volunteer_id')->where('b2b_user_id','=',\Session::get('userID'));
        $product = \DB::table('product_sale')->join('product','product.product_id','=','product_sale.product_id')->where('b2b_user_id','=',\Session::get('userID'));
        $user = \DB::table('b2b_user')->where('b2b_user_id','=',\Session::get('userID'))->get();
        return view('user.myAccount',compact('donation','volunteer','product','user'));
    }
    
    public function viewTopNGOs(){
        \Session::put('page','Top NGO(s)-B2B');
        $ngoList = \DB::table('topngo')->join('organisation','organisation.org_id','=','topngo.org_id')->orderBy('grades','desc')->take(10)->get();
        return view('user.topVisited')->with('ngoList',$ngoList);
    }

    public function viewAllNGOs(){
        \Session::put('page','Browse All NGO(s)-B2B');
        $ngoList = \DB::table('organisation')->orderBy('status','desc')->get();
        return view('user.allNGOs')->with('ngoList',$ngoList);
    }

    public function editUserDetails(Request $request){
        $rules = array(
            'name' => 'required',
            'mobile' => 'required|numeric',
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }
        else{
            $name = $request->name;
            $email = $request->email;
            $mobile = $request->mobile;
            $password = hash('sha512',hash('md5',$request->password));
            $id = \DB::table('b2b_user')->update(
                ['name' => $name, 'mobile_no' => $mobile]
            );
            \Session::flash('flash_message','Congrats! Details successfully updates');
            \Session::put('userName',$request->name);
            return redirect()->back();
        }
    }

    public function viewBuyProduct($id){
            if(\Session::has('userSession')){
                $product = \DB::table('product')->where('product_id', '=', $id)->get();
                return view('user.purchaseProduct',compact('product'));
            }
            else{
                \Session::flash('error_message','You must be logged in to continue');
                return Redirec::to('user/log');
            }
    }
    public function buyProduct(Request $request){
        if(\Session::has('userSession')){
            \Session::flash('flash_message','Thank you for your purchase! You"ll soon get your product delivered at your doorstep');
            \DB::table('product_sale')->insert([
                ['b2b_user_id'=>\Session::get('userID'),'org_id'=>$request->org_id,'product_id'=>$request->pid,'cost'=>'100']
            ]);
            echo $request->pcost;
            return Redirect::to('microwebsite/'.$request->org_id);
        }
        else{
            \Session::flash('error_message','You must be logged in to continue');
            return Redirec::to('user/log');
        }
    }

}