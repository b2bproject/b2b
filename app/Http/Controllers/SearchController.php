<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
class SearchController extends Controller{

    public function userSearch(Request $request){
        $rules = array(
            'search' => 'AlphaNum'
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }
        else{
            $searchQuery = Input::get('search');
            $org = \DB::table('organisation')->where('name','like',"%$searchQuery%")->count();
            $locality = \DB::table('organisation')->where('locality','like',"%$searchQuery%")->count();
            $product = \DB::table('product')->where('product_name','like',"%$searchQuery%")->count();
            if($org>0){
                $org = \DB::table('organisation')->where('name','like',"%$searchQuery%")->get();
                \Session::put('searchOrg',$org);
            }
            if($locality>0){
                $locality = \DB::table('organisation')->where('locality','like',"%$searchQuery%")->get();
                \Session::put('searchLocality',$locality);
            }
            if($product>0){
                $product = \DB::table('product')->where('product_name','like',"%$searchQuery%")->get();
                \Session::put('searchProduct',$product);
            }
            return view('search.userSearch');
        }
    }

}
