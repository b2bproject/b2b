<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller {
    public function index(){
        if(!Session::has('userSession')){
            return view('errors.503');
        }
        else if(!Session::get('userStatus')=='2'){
            return Redirect::to('/');
        }
        \Session::put('page','Admin Panel-B2B');
        if(\Session::get('contentType')=='topNGO' || \Session::get('contentType')=='featuredNGO'){
            return view('admin.index');
        }
        else{
            $ngoList = \DB::table('organisation')->select('*')->Paginate(10);
            \Session::put('contentType','allNGO');
            return view('admin.index',compact('ngoList'));
        }
    }

    public function generateTopNGOs(){
        if(!Session::has('userSession')){
            return view('errors.503');
        }
        else if(!Session::get('userStatus')=='2'){
            return Redirect::to('/');
        }
        $orgCount = \DB::table('organisation')->count();
        $check=true;
        for($i = 0; $i < $orgCount; $i++){
            $donationUser = \DB::table('donation_user')->where('org_id','=',$i)->count();
            $volunteerUser = \DB::table('volunteer_user')->where('org_id','=',$i)->count();
            $orgView = \DB::table('org_view')->where('org_id','=',$i)->count();
            $productSale = \DB::table('product_sale')->where('org_id','=',$i)->count();
            /*
             * Donation is multiplied by a factor of 20
             * Volunteer joined is multiplied by a factor of 25
             * Product sold is multiplied by a factor of 20
             * Microwebsite views is multiplied by a factor of 20
             * This accounts for a total of multiplication factor of 65
             * The grades if found >0 are added to the topngo table by truncating the previous list
             * They can then be listed anywhere, like at the admin panel or for the end users to show them the list of Top NGOs
             */
            $donationRate = $donationUser*20;
            $volunteerRate = $volunteerUser*25;
            $productRate = $productSale*15;
            $viewRate = $orgView*5;
            $total = $donationRate+$volunteerRate+$productRate+$viewRate;
            if($total>0){
                if($check){
                    \DB::table('topngo')->truncate();
                    $check = false;
                    echo $check;
                }
                $valid = \DB::table('topngo')->insert([
                    ['org_id' => $i,'grades' => $total]
                ]);
            }
        }
        \Session::flash('flash_message','NGO(s) have been re ranked based on their grades derived from the algorithm');
        return redirect()->back();
    }

    public function generateFeaturedNGOs(Request $request){
        if(!Session::has('userSession')){
            return view('errors.503');
        }
        else if(!Session::get('userStatus')=='2'){
            return Redirect::to('/');
        }
        $selection[] = $request->featured;
        $count=0;
        foreach($request->featured as $select) {
            //echo count($selection);
            if($count>10){
                break;
            }
            \DB::table('organisation')
                ->where('org_id', $select)
                ->update(['flag' => 1]);
            $count++;
        }
        \Session::flash('flash_message','Selected NGO(s) were made featured');
        return redirect()->back();
    }

    public function viewAll(){
        if(!Session::has('userSession')){
            return view('errors.503');
        }
        else if(!Session::get('userStatus')=='2'){
            return Redirect::to('/');
        }
        $ngoList = \DB::table('organisation')->select('*')->Paginate(10);
        \Session::put('contentType','allNGO');
        return Redirect::to('admin/index')->with('ngoList',$ngoList);
    }

    public function topNGOs(){
        if(!Session::has('userSession')){
            return view('errors.503');
        }
        else if(!Session::get('userStatus')=='2'){
            return Redirect::to('/');
        }
        $topNGO = \DB::table('topngo')->join('organisation','topngo.org_id','=','organisation.org_id')->select('*')->Paginate(10);
        \Session::put('contentType','topNGO');
        return view('admin.index')->with('topNGO',$topNGO);
    }
    
    public function featuredNGOs(){
        if(!Session::has('userSession')){
            return view('errors.503');
        }
        else if(!Session::get('userStatus')=='2'){
            return Redirect::to('/');
        }
        $featured = \DB::table('organisation')->select('*')->where('flag','=',1)->Paginate(10);
        \Session::put('contentType','featuredNGO');
        return view('admin.index')->with('featured',$featured);        
    }

    public function removeFeaturedTag(Request $request){
        if(!Session::has('userSession')){
            return view('errors.503');
        }
        else if(!Session::get('userStatus')=='2'){
            return Redirect::to('/');
        }
        $count=0;
        foreach($request->unfeature as $select) {
            \DB::table('organisation')
                ->where('org_id', $select)
                ->update(['flag' => 0]);
            $count++;
        }
        \Session::flash('warning_message','Selected NGO(s) were made unfeatured');
        return redirect()->back();
    }
}