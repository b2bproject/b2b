<?php

namespace App\Http\Controllers;

use App\Country;
use App\Organisation;
use App\Product;
use App\Donation;
use App\Volunteer;
use Illuminate\Http\Request;
//use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class OrganisationController extends Controller
{
    public function index(){
        if(!Session::has('userSession')){
            return Redirect::to('/');
        }
        if(Session::has('userSession')){
            if(Session::get('userStatus')=='0' || Session::get('userStatus')=='2'){
                $countries = \DB::table('country')->lists('name', 'country_id');
                $states = \DB::table('state')->lists('name', 'state_id');
                $works = \DB::table('type_of_work')->lists('type','type_of_work_id');
                \Session::put('page','Add NGO-B2B');
                return view('NGO.ngo')->with('countries',$countries)->with('states',$states)->with('works',$works);
            }
            else if(Session::get('userStatus')=='1'){
                \Session::error('error_message','You can register only 1 NGO');
                return redirect()->back();
            }
        }
    }

    public function addNGO(Request $request){
        //$organisation = new Organisation($request->all());
            //Organisation::create($request->all());
        if(!Session::has('userSession') || Session::get('userStatus')=='1'){
            return redirect()->back();
        }

        $rules = array(
            'name' => 'required|unique:organisation',
            'license_no' => 'required',
            'primary_email' => 'required|unique:organisation',
            'contact_no' => 'required|unique:organisation|numeric',
            'president_name' => 'required',
            'description' => 'required|min:20|max:1000',
            'address'=>'required|min:10',
            'locality' => 'required',
            'city' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'type_of_work_id' => 'required',
            'logo' => 'required'
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }
        else {
            $file = Input::file('logo');
            $destinationPath = 'public/uploads';
            $image = $file->getClientOriginalName();
            $logo = Session::get('userID').'_'.$image;
            Input::file('logo')->move($destinationPath, $logo);
            $check = \DB::table('organisation')->insert([
                ['name' => $request->name, 'president_name' => $request->president_name, 'description' => $request->description, 'license_no' => $request->license_no, 'address' => $request->address,
                    'locality'=>$request->locality, 'city'=>$request->city, 'state_id'=>$request->state_id,'country_id'=>$request->country_id,'type_of_work_id'=>$request->type_of_work_id,'primary_email'=>$request->primary_email,
                    'secondary_email'=>$request->secondary_email,'contact_no'=>$request->contact_no,'flag'=>0,'status'=>1,'b2b_user_id'=>Session::get('userID'),'website'=>$request->website,'logo'=>$logo]
            ]);
            if($check==1){
                if(Session::get('userStatus')=='0') {
                    \DB::table('b2b_user')->where('b2b_user_id', '=', Session::get('userID'))->update(['status' => 1]);
                    $status = \DB::table('b2b_user')->where('b2b_user_id', '=', Session::get('userID'))->pluck('status');
                    Session::put('userStatus', $status);
                }
                \Session::flash('flash_message', 'Your NGO details have been added');
            }
            else{
                \Session::flash('error_message', 'Your NGO details could not be added! Please try again later or contact us on email(at)ngoconnect(dot)com');
            }
            return Redirect::to('/');
        }
    }

    public function editNGO() {
        if(\Session::get('userStatus')=='2'){
            $id = \Session::get('adminOrgID');
            $organ = \DB::table('organisation')->where('org_id','=',$id)->get();//Organisation::where('org_id', $id)->get();
            $countries = \DB::table('country')->lists('name', 'country_id');
            $states = \DB::table('state')->lists('name', 'state_id');
            $works = \DB::table('type_of_work')->lists('type', 'type_of_work_id');
            \Session::put('page','Edit NGO-B2B');
            return view('NGO.editngo', compact('organ','countries','states','works'));
        }
        else if(\Session::get('userStatus')=='1'){
            $id = \Session::get('orgID');
            $organ = \DB::table('organisation')->where('org_id','=',$id)->get();
            $countries = \DB::table('country')->lists('name', 'country_id');
            $states = \DB::table('state')->lists('name', 'state_id');
            $works = \DB::table('type_of_work')->lists('type', 'type_of_work_id');
            \Session::put('page','Edit NGO-B2B');
            return view('NGO.editngo', compact('organ','countries','states','works'));
        }
        else{
            return view('errors.503');
        }
    }


    public function updateNGO($org_id, Requests\OrganisationRequest $request){
       if(!Session::has('userSession') || Session::get('userStatus')=='0'){
           return view('errors.503');
       }
        $input = $request->all();
        //echo $input;
        \DB::table('organisation')
            ->where('org_id', $org_id)
            ->update($input);

       // $organ->fill($input)->save();

        \Session::flash('flash_message', 'Organisation successfully updated!');

        $countries = \DB::table('country')->lists('name', 'country_id');
        $states = \DB::table('state')->lists('name', 'state_id');
        $works = \DB::table('type_of_work')->lists('type', 'type_of_work_id');
        //return view('NGO.ngo', compact('countries','states','works'));
        return redirect()->back();
    }




    public function displayAddNewsView(){
        return view('orgAdmin.addNews');
    }

    public function addNews(Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        $data = array(
            'name' => 'orgAdmin.addNews'
        );
        $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        if($request->hasFile('image')){
            if(($request->date)!=null) {
                $file = Input::file('image');
                $destinationPath = 'public/uploads';
                $name = $file->getClientOriginalName();
                $title = $request->title;
                $description = $request->description;
                $date = $request->date;
                \DB::table('news_highlight')->insert([
                    ['title' => $title, 'img' => $img_pre.''.$name, 'description' => $description, 'org_id' => $org_id, 'date' => $date]]);
                Input::file('image')->move($destinationPath, $img_pre.''.$name);
                //Product::create($request->all());
                \Session::flash('flash_message', 'Your News Highlights have been added');
                $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', $org_id)->Paginate(5);
                return redirect()->back()->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('productType',$productType)->with('image',Session::get('image'));
            }
            else{
                \Session::flash('error_message', 'Date filed is empty');
                return redirect()->back()->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
            }
        }
        else{
            \Session::flash('error_message', 'Image field is empty');
            return redirect()->back()->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
        }
    }

    public function editNews($n_id) {
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        $news = \DB::table('news_highlight')->select('*')->where('news_highlight_id', '=', $n_id )->get();
        return view('orgAdmin.editNews',compact('news'));
    }

    public function updateNews($n_id,Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }

        if (isset($_POST['deletebtn'])) {
            \DB::table('news_highlight')->where('news_highlight_id', $n_id)->delete();
            \Session::flash('flash_message', 'Your News has been deleted');
            return Redirect::to('orgAdmin/index/'.$org_id);

        } else {
            if ($request->hasFile('image')) {
                $file = Input::file('image');
                $destinationPath = 'public/uploads';
                $name = $file->getClientOriginalName();
                $title = $request->title;
                $image = $request->image;
                $description = $request->description;
                $date = $request->date;

                \DB::table('news_highlight')->where('news_highlight_id', $n_id)->update(['title' => $title, 'img' => $img_pre.''.$name, 'date' => $date, 'description' => $description, 'org_id' => $org_id]);
                Input::file('image')->move($destinationPath, $img_pre.''.$name);
                \Session::flash('flash_message', 'Your News has been updated');
                return Redirect::to('orgAdmin/index/'.$org_id);
            } else {
                $filename = $request->title;
                $title = $request->title;
                $description = $request->description;
                $date = $request->date;
                \DB::table('news_highlight')->where('news_highlight_id', $n_id)->update(['title' => $title, 'date' => $date, 'description' => $description, 'org_id' => $org_id]);
                \Session::flash('flash_message', 'Your News has been updated');
                return Redirect::to('orgAdmin/index/'.$org_id);
            }
        }
    }




    public function displayAddProjectView(){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        return view('orgAdmin.addProject');
    }

    public function addProject(Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        if($request->hasFile('image')){
            $file = Input::file('image');
            $destinationPath = 'public/uploads';
            $name = $file->getClientOriginalName();
            $filename = $request->title;
            $title = $request->title;
            $description = $request->description;
            $date = $request->date;
            \DB::table('project')->insert([
                ['title' => $title, 'image' => $img_pre.''.$name, 'description' => $description, 'org_id' => $org_id]]);
            Input::file('image')->move($destinationPath, $img_pre.''.$name);
            \Session::flash('flash_message', 'Your Project has been added');
            $project = \DB::table('project')->select('*')->where('org_id', '=', $org_id)->Paginate(5);
            echo $org_id.'_'.$img_pre;
            return redirect()->back();
        }
        else{
            \Session::flash('error_message', 'Image is compulsory');
            return redirect()->back();
        }
    }

    public function editProject($proj_id) {
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        $project = \DB::table('project')->select('*')->where('project_id', '=', $proj_id )->get();
        return view('orgAdmin.editProject',compact('project'));
    }

    public function updateProject($proj_id,Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        if(isset($_POST['deletebtn'])) {
            \DB::table('project')->where('project_id',$proj_id)->delete();
            \Session::flash('flash_message', 'Your Project has been deleted');
            return redirect()->back();
        }
        else {
            if ($request->hasFile('image')) {
                $file = Input::file('image');
                $destinationPath = 'public/uploads';
                $name = $file->getClientOriginalName();
                $title = $request->title;
                $image = $request->image;
                $description = $request->description;
                $date = $request->date;
                \DB::table('project')->where('project_id', $proj_id)->update(['title' => $title, 'image' => $img_pre.''.$name, 'description' => $description]);
                Input::file('image')->move($destinationPath, $org_id.''.$name);
                \Session::flash('flash_message', 'Your Item has been updated');
                $project = \DB::table('project')->select('*')->where('org_id', '=', $org_id)->Paginate(5);
                return Redirect::to('orgAdmin/index/'.$org_id);
            } else {
                $filename = $request->title;
                $title = $request->title;
                $description = $request->description;
                \DB::table('project')->where('project_id', $proj_id)->update(['title' => $title, 'description' => $description, 'org_id' => $org_id]);
                \Session::flash('flash_message', 'Your Item has been updated');
                $project = \DB::table('project')->select('*')->where('org_id', '=', $org_id)->Paginate(5);
                return Redirect::to('orgAdmin/index/'.$org_id);
            }
        }
    }

    public function displayAddEventView(){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        return view('orgAdmin.addEvent');
    }

    public function addEvent(Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        $data = array(
            'name' => 'orgAdmin.addEvent'
        );
        if($request->hasFile('image')){
            if($request->date!=null) {
                if($request->venue!=null) {
                    $file = Input::file('image');
                    $destinationPath = 'public/uploads';
                    $name = $file->getClientOriginalName();
                    $title = $request->title;
                    $description = $request->description;
                    $venue = $request->venue;
                    $date = $request->date;
                    \DB::table('events')->insert([
                        ['title' => $title, 'img' => $img_pre.''.$name, 'venue' => $venue, 'date' => $date, 'description' => $description, 'org_id' => $org_id]]);
                    Input::file('image')->move($destinationPath, $img_pre.''.$name);
                    \Session::flash('flash_message', 'Your Event has been added');
                    $events = \DB::table('events')->select('*')->where('org_id', '=', $org_id)->Paginate(5);
                    return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
                }
                else{
                    \Session::flash('error_message', 'Venue field is empty');
                    return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
                }
            }
            else{
                \Session::flash('error_message', 'Date field is empty');
                return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
            }
        }
        else{
            \Session::flash('error_message', 'Image is compulsory');
            return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
        }
    }

    public function editEvent($event_id) {
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        $events = \DB::table('events')->select('*')->where('event_id', '=', $event_id )->get();
        return view('orgAdmin.editEvent',compact('events'));
    }

    public function updateEvent($event_id,Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }

        if (isset($_POST['deletebtn'])) {
            \DB::table('events')->where('event_id', $event_id)->delete();
            \Session::flash('flash_message', 'Your Event has been deleted');
            return Redirect::to('orgAdmin/index');

        } else {
            if ($request->hasFile('image')) {
                $file = Input::file('image');
                $destinationPath = 'public/uploads';
                $name = $file->getClientOriginalName();
                $title = $request->title;
                $image = $request->image;
                $description = $request->description;
                $date = $request->date;
                $venue = $request->venue;
                \DB::table('events')->where('event_id', $event_id)->update(['title' => $title, 'venue' => $venue, 'img' => $img_pre.''.$name, 'date' => $date, 'description' => $description]);
                Input::file('image')->move($destinationPath, $img_pre.''.$name);
                \Session::flash('flash_message', 'Your Event has been updated');
                return Redirect::to('orgAdmin/index/'.$org_id);
            } else {
                $filename = $request->title;
                $title = $request->title;
                $description = $request->description;
                $date = $request->date;
                $venue = $request->venue;

                \DB::table('events')->where('event_id', $event_id)->update(['title' => $title, 'venue' => $venue, 'date' => $date, 'description' => $description]);
                \Session::flash('flash_message', 'Your Event has been updated');
                return Redirect::to('orgAdmin/index/'.$org_id);
            }
        }
    }


    public function displayAddDonationView(){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        return view('orgAdmin.addDonationScheme');
    }

    public function addDonationScheme(Request $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        $data = array(
            'name' => 'orgAdmin.addDonatioScheme'
        );
        $rules = array(
            'title' => 'required',
            'amount' => 'required|numeric',
            'period' => 'required|numeric'
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'))->withErrors($validator);//->with('data',$data);
        }else {
            $donation = new Donation;
            $donation->scheme = $request->title;
            $donation->amount = $request->amount;
            $donation->period = $request->period;
            $donation->status = $request->status;
            $donation->org_id = $org_id;
            $donation->save();
            \Session::flash('flash_message','Donation Scheme has been successfully added');
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', $org_id)->Paginate(5);
            //return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
        }
    }

    public function editDonation($d_id) {
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        $don = \DB::table('donation')->select('*')->where('donation_id', '=', $d_id )->get();
        return view('orgAdmin.editDonationScheme',compact('don'));
    }

    public function updateDonation($donation_id,Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $org_id = \Session::get('adminOrgID');
        }
        if(isset($_POST['deletebtn'])) {
            \DB::table('donation')->where('donation_id',$donation_id)->delete();
            \Session::flash('flash_message', 'Your Donation Scheme has been deleted');
            return Redirect::to('orgAdmin/index/'.$org_id);
        }
        else {
            $title = $request->title;
            $amount = $request->amount;
            $period = $request->period;
            $status = $request->status;

            \DB::table('donation')->where('donation_id', $donation_id)->update(['scheme' => $title, 'amount' => $amount, 'period' => $period, 'status' => $status, 'org_id' => $org_id]);

            \Session::flash('flash_message', 'Your Donation Scheme has been updated');
            return Redirect::to('orgAdmin/index/'.$org_id);
        }

    }

    public function displayAddVolunteerView(){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        return view('orgAdmin.addVolunteerProgramme');
    }

    public function addVolunteerProgramme(Request $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $org_id = \Session::get('adminOrgID');
        }
        $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        $data = array(
            'name' => 'orgAdmin.addVolunteerProgramme'
        );
        $rules = array(
            'title' => 'required',
            'description' => 'required|max:1000',
            'status' => 'required'
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data);
        }
        else {
            $volunteer = new Volunteer;
            $volunteer->title = $request->title;
            $volunteer->description = $request->description;
            $volunteer->status = $request->status;
            $volunteer->org_id = $org_id;
            $volunteer->save();
            \Session::flash('flash_message','Volunteering Programme added successfully');
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', $org_id)->Paginate(5);
            return redirect()->back();
        }
    }

    public function editVolunteerProgramme($v_id) {
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        $vol = \DB::table('volunteer')->select('*')->where('volunteer_id', '=', $v_id )->get();
        return view('orgAdmin.editVolunteerProgramme',compact('vol'));
    }

    public function updateVolunteerProgramme($v_id,Requests\AddItemRequest $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $org_id = \Session::get('adminOrgID');
        }
        if(isset($_POST['deletebtn'])) {
            \DB::table('volunteer')->where('volunteer_id',$v_id)->delete();
            \Session::flash('flash_message', 'Your Volunteer Programme has been deleted');
            return Redirect::to('orgAdmin/index/'.$org_id);
        }
        else {
            $title = $request->title;
            $des = $request->description;
            $status = $request->status;
            \DB::table('volunteer')->where('volunteer_id', $v_id)->update(['title' => $title, 'description' => $des, 'status' => $status, 'org_id' => $org_id]);
            \Session::flash('flash_message', 'Your Volunteer Programme has been updated');
            return Redirect::to('orgAdmin/index/'.$org_id);
        }
    }


    public function imagesView(){
        if(\Session::has('userSession')) {
            if (\Session::get('userStatus') == '1') {
                $images = \DB::table('image')->select('*')->where('org_id','=',\Session::get('orgID'))->get();
                if($images==null){
                    return view('orgAdmin.addImages');
                }
                else{
                    return view('orgAdmin.editImages')->with('image',$images);
                }
            }
            if (\Session::get('userStatus') == '2') {
                $images = \DB::table('image')->select('*')->where('org_id','=',\Session::get('adminOrgID'))->get();
                if($images==null){
                    return view('orgAdmin.addImages');
                }
                else{
                    return view('orgAdmin.editImages')->with('image',$images);
                }

            } else {
                \Session::flash('error_message', 'You are not authorized to view this page');
                return Redirect::to('/');
            }
        }
        else{
            \Session::flash('error_message','You must be logged in to  view this page');
        }
    }



    public function addImages(Request $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        $data = array(
            'name' => 'orgAdmin.editImages'
        );
        $rules = array(
            'image1' => 'required',
            'image2' => 'required'
        );
        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
            return redirect()->back();
        }
        else{
            $image1 = Input::file('image1');
            $image2 = Input::file('image2');
            $destinationPath = 'public/uploads';
            $image1_name = $image1->getClientOriginalName();
            $image2_name = $image2->getClientOriginalName();
            Input::file('image1')->move($destinationPath, $img_pre.''.$image1_name);
            Input::file('image2')->move($destinationPath, $img_pre.''.$image2_name);
            \DB::table('image')->insert([
                ['img_name' => $img_pre.''.$image1_name, 'org_id' => $org_id],
                ['img_name' => $img_pre.''.$image2_name, 'org_id' => $org_id]
            ]);
            if(isset($request->image3)){
                $image3 = Input::file('image3');
                $image3_name = $image3->getClientOriginalName();
                Input::file('image3')->move($destinationPath,$img_pre.''.$image3_name);
                \DB::table('image')->insert([
                   ['img_name' => $img_pre.''.$image3_name, 'org_id' => $org_id]
                ]);
            }
            if(isset($request->image4)){
                $image4 = Input::file('image4');
                $image4_name = $image4->getClientOriginalName();
                Input::file('image4')->move($destinationPath,$img_pre.''.$image4_name);
                \DB::table('image')->insert([
                    ['img_name' => $img_pre.''.$image4_name, 'org_id' => $org_id]
                ]);
            }
            if(isset($request->image5)){
                $image5 = Input::file('image5');
                $image5_name = $image5->getClientOriginalName();
                Input::file('image5')->move($destinationPath,$img_pre.''.$image5_name);
                \DB::table('image')->insert([
                    ['img_name' => $img_pre.''.$image5_name, 'org_id' => $org_id]
                ]);
            }
            \Session::flash('flash_message','Images added successfully');
            $image = \DB::table('image')->select('*')->where('org_id', '=', $org_id)->get();
            return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data)->with('image',$image);
        }
    }

    public function editImages(Request $request){
        if(!Session::has('userSession') || Session::get('userStatus')=='0'){
            return view('errors.503');
        }
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        $data = array(
            'name' => 'orgAdmin.editImages'
        );
        $destinationPath = 'public/uploads';
        $count = \DB::table('image')->where('org_id','=',$org_id)->count();
        if(isset($request->image1)){
            $image1 = Input::file('image1');
            $image1_name = $image1->getClientOriginalName();
            Input::file('image1')->move($destinationPath,$img_pre.''.$image1_name);
            \DB::table('image')->where('img_name',$request->oldImage1)->update(['img_name' => $img_pre.''.$image1_name]);
        }
        if(isset($request->image2)){
            $image2 = Input::file('image2');
            $image2_name = $image2->getClientOriginalName();
            Input::file('image2')->move($destinationPath,$img_pre.''.$image2_name);
            \DB::table('image')->where('img_name',$request->oldImage2)->update(['img_name' => $img_pre.''.$image2_name]);
        }
        if(isset($request->image3)){
            $image3 = Input::file('image3');
            $image3_name = $image3->getClientOriginalName();
            Input::file('image3')->move($destinationPath,$img_pre.''.$image3_name);
            \DB::table('image')->where('img_name',$request->oldImage3)->update(['img_name' => $img_pre.''.$image3_name]);
        }
        if(isset($request->image4)){
            $image4 = Input::file('image4');
            $image4_name = $image4->getClientOriginalName();
            Input::file('image4')->move($destinationPath,$img_pre.''.$image4_name);
            if($count<43){
                \DB::table('image')->insert([
                    ['img_name' => $img_pre.''.$image4_name, 'org_id' => $org_id]
                ]);
            }
            else if($count==4 || $count==5){
                \DB::table('image')->where('img_name',$request->oldImage4)->update(['img_name' => $img_pre.''.$image4_name]);
            }

        }
        if(isset($request->image5)){
            $image5 = Input::file('image5');
            $image5_name = $image5->getClientOriginalName();
            Input::file('image5')->move($destinationPath,$img_pre.''.$image5_name);
            if($count<5){
                \DB::table('image')->insert([
                    ['img_name' => $img_pre.''.$image5_name, 'org_id' => $org_id]
                ]);
            }
            else {
                \DB::table('image')->where('img_name', $request->oldImage5)->update(['img_name' => $img_pre . '' . $image5_name]);
            }
        }
        \Session::flash('flash_message','Image(s) changed successfully');
        $image = \DB::table('image')->select('*')->where('org_id', '=', $org_id)->get();
        return redirect()->back();//->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));//->with('data',$data)->with('image',$image);
    }

    public function viewLogo(){
        if(\Session::get('userStatus')=='2'){
            $id = \Session::get('adminOrgID');
            $logo = \DB::table('organisation')->where('org_id','=',$id)->pluck('logo');
            \Session::put('logo',$logo);
            return view('orgAdmin.addLogo');
        }
        else if(\Session::get('userStatus')=='1'){
            $id = \Session::get('orgID');
            $logo = \DB::table('organisation')->where('org_id','=',$id)->pluck('logo');
            \Session::put('logo',$logo);
            return view('orgAdmin.addLogo');
        }
        else{
            return view('errors.503');
        }
    }

    public function addLogo(Request $request){
        $destinationPath = 'public/uploads';
        if(\Session::get('userStatus')=='2') {
            $id = \Session::get('adminOrgID');
            if (isset($request->changelogo)) {
                \Session::flash('info_message','Your Logo has been changed successful');
                $logo = Input::file('changelogo');
                $logo_name = $logo->getClientOriginalName();
                Input::file('changelogo')->move($destinationPath, $id.'_' . $logo_name);
                $check = \DB::table('organisation')->where('org_id', '=', $id)->update(['logo' => $id.'_' . $logo_name]);
            } else if (isset($request->addlogo)) {
                $logo = Input::file('addlogo');
                $logo_name = $logo->getClientOriginalName();
                Input::file('addlogo')->move($destinationPath, $id.'_' . $logo_name);
                $check = \DB::table('organisation')->where('org_id', '=', $id)->update(['logo' => $id.'_' . $logo_name]);
            } else {
                \Session::flash('warning_message', 'Please select a new logo to make changes');
            }
        }
        else if(\Session::get('userStatus')=='1'){
            $id = \Session::get('orgID');
            if (isset($request->changelogo)) {
                \Session::flash('info_message','Your Logo has been changed successful');
                $logo = Input::file('changelogo');
                $logo_name = $logo->getClientOriginalName();
                Input::file('changelogo')->move($destinationPath, $id.'_' . $logo_name);
                $check = \DB::table('organisation')->where('org_id', '=', $id)->update(['logo' => $id.'_' . $logo_name]);
            } else if (isset($request->addlogo)) {
                $logo = Input::file('addlogo');
                $logo_name = $logo->getClientOriginalName();
                Input::file('addlogo')->move($destinationPath, $id.'_' . $logo_name);
                $check = \DB::table('organisation')->where('org_id', '=', $id)->update(['logo' => $id.'_' . $logo_name]);
            } else if (isset($request->removeLogo)) {
                $check = \DB::table('organisation')->where('org_id', '=', $id)->update(['logo' => null]);
            } else {
                \Session::flash('warning_message', 'Please select a new logo to make changes');
            }
        }
        else{
            return view('errors.503');
        }
        return redirect()->back();
    }

    public function home($id){
        if(\Session::get('userStatus')=='2'){
            \Session::put('page','Admin Panel-B2B');
            \Session::put('logo', \DB::table('organisation')->where('org_id','=',$id)->pluck('logo'));
            Session::put('image', \DB::table('image')->select('*')->where('org_id','=',\Session::get('adminOrgID'))->get());
            \Session::put('adminOrgID',$id);
            $project = \DB::table('project')->select('*')->where('org_id', '=', $id)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', $id)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', $id)->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', $id)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', $id)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', $id)->Paginate(5);
            $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
            return view('orgAdmin.index',compact('project'))->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('productType',$productType)->with('images',\Session::get('image'));
        }
        else if(\Session::get('userStatus')=='1'){
            \Session::put('page','NGO Panel-B2B');
            Session::put('image', \DB::table('image')->select('*')->where('org_id','=',\Session::get('orgID'))->get());
            \Session::put('logo', \DB::table('organisation')->where('org_id','=',\Session::get('orgID'))->pluck('logo'));
            $project = \DB::table('project')->select('*')->where('org_id', '=', \Session::get('orgID'))->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', \Session::get('orgID'))->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', \Session::get('orgID'))->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', \Session::get('orgID'))->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', \Session::get('orgID'))->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', \Session::get('orgID'))->Paginate(5);
            $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
            return view('orgAdmin.index',compact('project'))->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('productType',$productType)->with('image',Session::get('image'));
        }
        else {
            return view('errors.503');
        }
    }

    public function routeOrgAdmin(Request $request){
        /*$r = $request->button;
        if(isset($r)=='Project'){
            return view('orgAdmin.addNews');
        }*/
        if(isset($request->project)){
            $data = array(
                'name'  => 'orgAdmin.addProject',
            );
            $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
            return view('orgAdmin.index')->with('data',$data)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));
        }
        else if(isset($request->event)){
            $data = array(
                'name'  => 'orgAdmin.addEvent',
            );
            $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
            return view('orgAdmin.index')->with('data',$data)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));
        }
        else if(isset($request->news)){
            $data = array(
                'name' => 'orgAdmin.addNews',
            );
            $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
            return view('orgAdmin.index')->with('data',$data)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer)->with('image',Session::get('image'));
        }
        else if(isset($request->product)){
            $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
            $data = array(
                'name' => 'orgAdmin.addProduct',
            );
            $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
            return view('orgAdmin.index')->with('data',$data)->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer);
        }
        else if(isset($request->donation)){
            $data = array(
                'name' => 'orgAdmin.addDonationScheme'
            );
            $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
            return view('orgAdmin.index')->with('data',$data)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer);
        }
        else if(isset($request->volunteer)){
            $data = array(
                'name' => 'orgAdmin.addVolunteerProgramme'
            );
            $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
            return view('orgAdmin.index')->with('data',$data)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer);
        }
        else if(isset($request->editImages)){
            $image = \DB::table('image')->select('*')->where('org_id', '=', 1)->get();
            if(!($image==null)) {
                $data = array(
                    'name' => 'orgAdmin.editImages'
                );
                //if images are added before and need to be changed
                $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
                return view('orgAdmin.index')->with('data', $data)->with('image',$image)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer);
            }
            else{
                $data = array(
                    'name' => 'orgAdmin.addImages'
                );
                //if images are to be added for the first time
                $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $product = \DB::table('product')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
                $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
                return view('orgAdmin.index')->with('data',$data)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer);
            }
        }
        else if(isset($request->editDetails)){
            $data = array(
                'name' => 'orgAdmin.addImages'
            );
            //if images are to be added for the first time
            $project = \DB::table('project')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $events = \DB::table('events')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $product = \DB::table(  'product')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1)->Paginate(5);
            $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1)->Paginate(5);
            return view('orgAdmin.index')->with('data',$data)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer);
        }
    }

    function viewStatistics(){
        if(!\Session::has('userSession') || \Session::get('userStatus')=='0'){
            echo \Session::get('userStatus');
        }
        \Session::put('page','NGO Statistics-B2B');
        return view('orgAdmin.statistics');
    }
}
