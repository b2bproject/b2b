<?php

namespace App\Http\Controllers;

use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        $project = \DB::table('project')->select('*')->where('org_id', '=', 1);//->Paginate(5);
        $events = \DB::table('events')->select('*')->where('org_id', '=', 1);//->Paginate(5);
        $news = \DB::table('news_highlight')->select('*')->where('org_id', '=', 1);//->Paginate(5);
        $product = \DB::table('product')->select('*')->where('org_id', '=', 1);//->Paginate(5);
        $donation = \DB::table('donation')->select('*')->where('org_id', '=', 1);//->Paginate(5);
        $volunteer = \DB::table('volunteer')->select('*')->where('org_id', '=', 1);//->Paginate(5);
        return view('orgAdmin.addProduct')->with('productType',$productType)->with('project',$project)->with('events',$events)->with('news',$news)->with('product',$product)->with('donation',$donation)->with('volunteer',$volunteer);//->with('data',$data);
    }

    /**
     * @param Requests\ProductRequest $request
     * @return $this
     */
    public function create(Requests\ProductRequest $request){
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        if($request->hasFile('image')){
            $file = Input::file('image');
            $destinationPath = 'public/uploads';
            $filename = $file->getClientOriginalName();
            $name = $request->product_name;
            $product_type = $request->product_type_id;
            $amount = $request->cost;
            $availability = $request->availability;
            $description = $request->description;

            \DB::table('product')->insert([
                ['product_name' => $name, 'image' => $img_pre.''.$filename, 'description' => $description, 'cost' => $amount, 'availability' => $availability, 'org_id' => $org_id, 'product_type_id' => $product_type]
            ]);

            Input::file('image')->move($destinationPath, $img_pre.''.$filename);
            \Session::flash('flash_message', 'Your Product details has been added');
        }

        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        $data = array(
            'name' => 'orgAdmin.addProduct'
        );
        return redirect()->back();
    }

    public function edit($id)
    {
        $product = \DB::table('product')->select('*')->where('product_id', '=', $id )->get();
        $productType = \DB::table('product_type')->lists('product_type', 'product_type_id');
        return view('orgAdmin.editProduct',compact('product','productType'));

    }

    /**
     * Update the specified resource in storage.
     * @param $product_id
     * @param Requests\ProductRequest|Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update($product_id,Requests\ProductRequest $request){
        if(\Session::has('orgID')){
            $img_pre = \Session::get('orgID').'_';
            $org_id = \Session::get('orgID');
        }
        else if(\Session::has('adminOrgID')){
            $img_pre = \Session::get('adminOrgID').'_';
            $org_id = \Session::get('adminOrgID');
        }
        if (isset($_POST['deletebtn'])) {
            \DB::table('product')->where('product_id', $product_id)->delete();
            \Session::flash('flash_message', 'Your Product has been deleted');
            return Redirect::to('orgAdmin/index/'.$org_id);

        } else {
            if ($request->hasFile('image')) {
                $file = Input::file('image');
                $destinationPath = 'public/uploads';
                $filename = $file->getClientOriginalName();
                $name = $request->product_name;
                $product_type = $request->product_type_id;
                $amount = $request->cost;
                $availability = $request->availability;
                $description = $request->description;

                \DB::table('product')->where('product_id', $product_id)->update(
                    ['product_name' => $name, 'image' => $img_pre.''.$filename, 'description' => $description, 'cost' => $amount, 'availability' => $availability, 'product_type_id' => $product_type]
                );
                Input::file('image')->move($destinationPath, $img_pre.''.$filename);
                \Session::flash('flash_message', 'Your Product details has been updated');
                return Redirect::to('orgAdmin/index/'.$org_id);
            } else {
                $name = $request->product_name;
                $product_type = $request->product_type_id;
                $amount = $request->cost;
                $availability = $request->availability;
                $description = $request->description;
                \DB::table('product')->where('product_id', $product_id)->update(
                    ['product_name' => $name, 'description' => $description, 'cost' => $amount, 'availability' => $availability, 'product_type_id' => $product_type]
                );
                \Session::flash('flash_message', 'Your Product details has been updated');
                return Redirect::to('orgAdmin/index/'.$org_id);
            }
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
