<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OrganisationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            //'license_no' => 'required|unique:organisation',
            'president_name' => 'required',
            //'primary_email' => 'required|email|unique:organisation',
            'description' => 'required',
            'address' => 'required',
            'locality' => 'required',
            'city' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
            'type_of_work_id' => 'required',
            //'contact_no' => 'required|unique:organisation',
            //'website' => 'unique:organisation'
        ];
    }
}
