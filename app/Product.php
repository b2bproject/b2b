<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Product extends Model{

    protected $fillable = [
        'product_name',
        'cost',
        'description',
        'image',
        'availability',
        'product_type_id'
    ];

    public $table = 'product';

    protected $dates = ['updated_at'];

    public function scopePublished($query){
        $query->where('updated_at','<=',Carbon::now());
    }

    public function setPublishedAtAttribute($date){
        $this->attributes['updated_at'] = Carbon::createFromFormat('Y-m-d',$date);
    }

}
