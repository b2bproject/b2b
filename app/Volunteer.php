<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volunteer extends Model
{
    protected $fillable = [
        'title',
        'description',
        'status',
        'org_id'
    ];

    protected $table = "volunteer";

    public function user(){
        return $this->belongsTo('1');
    }
}
