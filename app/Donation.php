<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $fillable = [
        'scheme',
        'amount',
        'period',
        'status',
        'org_id'
    ];

    public $table = "donation";

    public function user(){
        return $this->belongsTo('1');
    }
}

