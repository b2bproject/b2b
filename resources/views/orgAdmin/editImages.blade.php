<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 10-03-2016
 * Time: 23:19
 */
?>
@extends('user.header')
@section('content')
<div class="container" id="bodyheading">
    <h1 class="page-header heading"><strong><span style="font-size: 140%; color: #3f45ad;">Edit Images&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h1>
    <p class="help-block">These images will appear in the slider</p>

    @include('partials/flash')
    @include('errors.lists')


    <div class="container" style="margin-bottom: 20px">
        <form action="index/editImagesSubmit" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" data-toggle="validator">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <?php $count=0; ?>
                    @foreach($image as $i)
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <?php $count++; echo $count; ?>
                            <img src="../public/uploads/{{$i->img_name}}" alt="" class="img-responsive img-thumbnail" title="{{$i->img_name}}" style="width: 100px; height: 100px">
                            <input type="hidden" name="oldImage<?php echo $count; ?>" value="{{$i->img_name}}">
                        </div>
                    @endforeach
                </div>

                <div class="col-md-6 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image1" class="control-label">Image 1</label>
                            <input type="file" name="image1" id="image1">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image2" class="control-label">Image 2</label>
                            <input type="file" name="image2" id="image2">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image3" class="control-label">Image 3</label>
                            <input type="file" name="image3" id="image3">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image4" class="control-label">Image 4</label>
                            <input type="file" name="image4" id="image4">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image5" class="control-label">Image 5</label>
                            <input type="file" name="image5" id="image5">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 10px">
                <div class="col-sm-offset-2">
                    <div class="col-sm-4 col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block" name="addImage">Changes Images</button>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <button type="reset" class="btn btn-default btn-block" name="resetImage">Reset form and fill again</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
