<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 10-03-2016
 * Time: 15:45
 */
?>
<div class="container" style="margin-bottom: 10px">
    <div class="row">
        <div class="col-xs-12">
            <h3 class="heading">
                Add details to your NGO
            </h3>
            <p class="help-block">Click on below buttons to add</p>
        </div>
        <div class="col-md-12">
            <div class="btn-group-justified" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#project" aria-expanded="false" aria-controls="project">
                        <small>Project</small>
                    </button>
                </div>
                <div class="btn-group" role="group">
                    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#event" aria-expanded="false" aria-controls="event">
                        <small>Event</small>
                    </button>
                </div>
                <div class="btn-group" role="group">
                    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#news" aria-expanded="false" aria-controls="news">
                        <small>News</small>
                    </button>
                </div>
                <div class="btn-group" role="group">
                    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#product" aria-expanded="false" aria-controls="product">
                        <small>Product</small>
                    </button>
                </div>
                <div class="btn-group" role="group">
                    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#donation" aria-expanded="false" aria-controls="donation">
                        <small>Donation Schemes</small>
                    </button>
                </div>
                <div class="btn-group" role="group">
                    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#volunteer" aria-expanded="false" aria-controls="volunteer">
                        <small>Volunteer Programmes</small>
                    </button>
                </div>
            </div>


                <div class="collapse" id="project">
                    <div class="well">
                        @include('orgAdmin.addProject')
                    </div>
                </div>
                <div class="collapse" id="event">
                    <div class="well">
                        @include('orgAdmin.addEvent')
                    </div>
                </div>
                <div class="collapse" id="news">
                    <div class="well">
                        @include('orgAdmin.addNews')
                    </div>
                </div>
                <div class="collapse" id="product">
                    <div class="well">
                        @include('orgAdmin.addProduct')
                    </div>
                </div>
                <div class="collapse" id="donation">
                    <div class="well">
                        @include('orgAdmin.addDonationScheme')
                    </div>
                </div>
                <div class="collapse" id="volunteer">
                    <div class="well">
                        @include('orgAdmin.addVolunteerProgramme')
                    </div>
                </div>
                <div class="collapse" id="editDetails">
                    <div class="well">

                    </div>
                </div>

        </div>
    </div>
</div>
