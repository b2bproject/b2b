<?php
/**
 * Created by PhpStorm.
 * User: sajnaninisha
 * Date: 31-03-2016
 * Time: 23:39
 */
?>
@extends('user.header')

@section('content')
<div class="container" id="bodyheading">
    <h4 class="heading"><strong><span style="font-size: 140%; color: #3f45ad;">Add Event&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h4>
    @include('partials/flash')
    @include('errors.lists')

    <div class="container">
        @foreach($events as $e)
        <form action="../events/{{$e->event_id}}" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" data-toggle="validator">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="title" class="control-label req">Title</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Event Title" value="@if(isset($e->title)){{$e->title}}@endif" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="date" class="control-label req">Event Date</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="text" class="form-control" id="datepicker" name="date" value="@if(isset($e->date)){{$e->date}}@endif" placeholder="Select a date">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="image" class="control-label req">Image</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <img src="../../../public/uploads/{{$e->img}}" alt="" class="img-responsive img-thumbnail" title="{{$e->img}}" style="width: 100px; height: 100px">
                                <input type="file" id="image" name="image">
                                <p class="help-block" style="font-size: 80%">Image size must be less than 2mb</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-11 col-xs-12">
                        <div class="form-group">
                            <label for="venue" class="control-label req">Venue</label>
                            <div class="input-group col-xs-12">
                                <textarea name="venue" id="venue" rows="2" class="form-control" placeholder="Enter venue">@if(isset($e->venue)){{$e->venue}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-11 col-xs-12">
                        <div class="form-group">
                            <label for="description" class="control-label req">Description</label>
                            <div class="input-group col-xs-12">
                                    <textarea name="description" id="description" rows="8" placeholder="Enter Event Description in upto 1000 characters"
                                              class="form-control" required>@if(isset($e->description)){{$e->description}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-offset-2">
                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-primary btn-block" name="addNews">Edit Event</button>
                            </div>
                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-danger btn-block" name="deletebtn" >Delete Event</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
            @endforeach
    </div>
</div>
@endsection