<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 14-04-2016
 * Time: 19:08
 */
?>
@extends('user.header')


@section('content')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <div class="container">
        <h1 class="page-header heading">
            {{\Session::get('org_name')}} Statistics
        </h1>
        <div class="col-md-12">
            <div id="container" style="width: 90%; height: 80%; margin: 0 auto">

            </div>
        </div>
    </div>
    <?php
        /*
         * Getting count of number of viewers of a microwebsite
         */
            if(\Session::get('userStatus')=='1'){
                $org_id = \Session::get('orgID');
            }
            else if(\Session::get('userStatus')=='2'){
                $org_id = \Session::get('adminOrgID');
            }
            echo $org_id;
            $org_view = DB::table('org_view')
                    ->select(DB::raw('count(org_id) as view_count, org_id'))
                    ->where('org_id', '=', $org_id)
                    ->groupBy(DB::raw('MONTH(timestamp)'))
                    ->get();
            $views = array();
            $count=1;
            foreach($org_view as $v){
                $views[] = $v->view_count;
            }
            /*
             * Getting count of products sold
             */
            $org_sales = DB::table('product_sale')
                    ->select(DB::raw('count(org_id) as sale_count, org_id'))
                    ->where('org_id', '=', $org_id)
                    ->groupBy(DB::raw('MONTH(date)'))
                    ->get();
            $sales = array();
            $count=1;
            foreach($org_sales as $s){
                $sales[] = $s->sale_count;
            }

            /*
             * Getting t count of donations received
             */
            $org_donation = DB::table('donation_user')
                    ->select(DB::raw('count(org_id) as donate_count, org_id'))
                    ->where('org_id', '=', $org_id)
                    ->groupBy(DB::raw('MONTH(donated_on)'))
                    ->get();
            $donations = array();
            $count=1;
            foreach($org_donation as $d){
                $donations[] = $d->donate_count;
            }

            /*
             * Getting count of volunteers joined
             */
            $org_volunteers = DB::table('volunteer_user')
                    ->select(DB::raw('count(org_id) as volunteer_count, org_id'))
                    ->where('org_id', '=', $org_id)
                    ->groupBy(DB::raw('MONTH(joined_on)'))
                    ->get();
            $volunteers = array();
            $count=1;
            foreach($org_volunteers as $v){
                $volunteers[] = $v->volunteer_count;
            }


    ?>
    <script language="JavaScript">

        $(document).ready(function() {
            var title = {
                text: '<strong>Monthly NGO Statistics</strong>'
            };
            var subtitle = {
                text: ''
            };
            var xAxis = {
                categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            };

            var yAxis= {
                min: -2,  //set min value
                max: 20,  //set max value
                tickInterval: 2, //set interval
                lineColor: '#FF0000',
                lineWidth: 1,
                title: {
                    text: '<strong>Count per month</strong>'

                },
                plotLines: [{
                    value: 0,
                    width: 3,
                    color: '#000080'
                }]
            };
            var tooltip = {
                valueSuffix: ''
            }

            var legend = {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            };

            var series =  [
                {
                    name: 'Volunteers',
                    data: [<?php echo join($volunteers, ","); ?>]
                },
                {
                    name: 'Donations',
                    data: [<?php echo join($donations, ","); ?>]
                },
                {
                    name: 'Products',
                    data: [<?php echo join($sales, ","); ?>]
                },
                {
                    name: 'Views',
                    data: [<?php echo join($views, ","); ?>]
                }
            ];

            var json = {};

            json.title = title;
            json.subtitle = subtitle;
            json.xAxis = xAxis;
            json.yAxis = yAxis;
            json.tooltip = tooltip;
            json.legend = legend;
            json.series = series;

            $('#container').highcharts(json);
        });
    </script>
@endsection
