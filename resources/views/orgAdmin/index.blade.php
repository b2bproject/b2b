<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 10-03-2016
 * Time: 15:45
 */
?>
@extends('user.header')

@section('content')
    <style>
        .req{
            color: #C20000;
        }
    </style>
    <div class="container" id="bodyheading">
        <h2 class="heading page-header"><span style="font-size: 140%; color: #3f45ad;">Welcome {{Session::get('org_name')}}</span></h2>
    </div>
    @include('orgAdmin.buttons')
    <div class="container">
        <div class="row">
            @include('partials.flash')
            @include('errors.lists')
        </div>
    </div>
    <hr>
    <div class="container" style="margin-bottom: 10px;">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading page-header">Added Items</h2>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#projects" aria-controls="projects" role="tab" data-toggle="tab">Projects</a></li>
                    <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>
                    <li role="presentation"><a href="#new" aria-controls="new" role="tab" data-toggle="tab">News and Highlights</a></li>
                    <li role="presentation"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">Products</a></li>
                    <li role="presentation"><a href="#donations" aria-controls="donations" role="tab" data-toggle="tab">Donation Schemes</a></li>
                    <li role="presentation"><a href="#volunteers" aria-controls="volunteers" role="tab" data-toggle="tab">Volunteer Programmes</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="projects">
                        <div class="col-md-12" style="margin-top: 5px">
                            <div class="table-responsive">
                                <table class="table-hover">
                                    @foreach($project as $p)
                                        <tr>
                                            <td rowspan="2" style="min-width: 150px"><img src="../../public/uploads/{{$p->image}}" alt=""
                                                                 class="img-responsive img-rounded" style="max-width: 100px"></td>
                                            <td><h3 class="heading"><a href="editProject/{{$p->project_id}}"> {{$p->title}} </a></h3></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>{{$p->description}}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div><span style="alignment: center"><?php echo $project->render();?></span></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="events">
                        <div class="col-md-12" style="margin-top: 5px">
                            <div class="table-responsive">
                                <table class="table-hover">
                                    @foreach($events as $e)
                                        <tr>
                                            <td rowspan="3" style="min-width: 150px">
                                                <img src="../../public/uploads/{{$e->img}}" alt=""
                                                     class="img-responsive img-rounded" style="max-width: 125px">
                                            </td>
                                            <td><h2><a href="editEvent/{{$e->event_id}}"> {{$e->title}} </a></h2></td>
                                            <td><p><strong>Event Date: </strong>{{$e->date}}</p></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><p><strong>Venue: </strong>{{$e->venue}}</p></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><p><strong>Description: </strong>{{$e->description}}</p></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div><span style="alignment: center"><?php echo $events->render();?></span></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="new">
                        <div class="col-md-12" style="margin-top: 5px">
                            <div class="table-responsive">
                                <table class="table-hover">
                                    @foreach($news as $n)
                                        <tr>
                                            <td rowspan="3" style="min-width: 150px">
                                                <img src="../../public/uploads/{{$n->img}}" alt=""
                                                     class="img-responsive img-rounded" style="max-height: 75px">
                                            </td>
                                            <td>
                                                <h3 class="heading"><a href="editNews/{{$n->news_highlight_id}}"> {{$n->title}} </a></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Date: </strong>{{$n->date}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Description: </strong>{{$n->description}}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div><span style="alignment: center"><?php echo $news->render();?></span></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="products">
                        <div class="col-md-12" style="margin-top: 5px">
                            <div class="table-responsive">
                                <table class="table-hover" style="color: #000">
                                    @foreach($product as $p)
                                        <?php $c=$p->product_type_id;?>
                                        <?php $cat = \DB::table('product_type')->where('product_type_id', $c)->pluck('product_type'); ?>
                                        <tr>
                                            <td rowspan="3" style="min-width: 150px">
                                                <img src="../../public/uploads/{{$p->image}}" alt=""
                                                     class="img-responsive img-rounded" style="max-width: 125px">
                                            </td>
                                            <td colspan="4">
                                                <h3 class="heading"><a href="editProduct/{{$p->product_id}}">{{$p->product_name}} </a></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <p>{{$p->description}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Cost: </strong>{{$p->cost}}  </p>
                                            </td>
                                            <td>
                                                <p><strong>Availability: </strong>{{$p->availability}}  </p>
                                            </td>
                                            <td>
                                                <p><strong>Category: </strong>{{$cat}}  </p>
                                            </td>
                                            <td>
                                                <p><strong>Added on: </strong>{{$p->created_at}}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div><span style="alignment: center"><?php echo $product->render();?></span></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="donations">
                        <div class="col-md-12" style="margin-top: 5px">
                            <div class="table-responsive">
                                <table class="table-hover">
                                    @foreach($donation as $d)
                                        <tr>
                                            <td>
                                                <h3 class="heading"><a href="editDonation/{{$d->donation_id}}">{{$d->scheme}} </a></h3>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <p><strong>Amount: </strong>Rs.{{$d->amount}}</p>
                                            </td>
                                            <td>
                                                <p><strong>Period: </strong>{{$d->period}}</p>
                                            </td>
                                            <td>
                                                <p><strong>Status: </strong>{{($d->status)==1?'Active':'Inactive'}}</p>
                                            </td>
                                            <td>
                                                <p><strong>Added On: </strong>{{$d->created_at}}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div><span style="alignment: center"><?php echo $donation->render();?></span></div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="volunteers">
                        <div class="col-md-12" style="margin-top: 5px">
                            <div class="table-responsive">
                                <table class="table-hover">
                                    @foreach($volunteer as $v)
                                        <tr>
                                            <td>
                                                <h3 class="heading"><a href="editVolunteerProgramme/{{$v->volunteer_id}}">{{$v->title}} </a></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>{{$v->description}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Status: </strong>{{($v->status)==1?'Active':'Inactive'}}</p>
                                            </td>
                                            <td>
                                                <p><strong>Added On: </strong>{{$v->created_at}}</p>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div><span style="alignment: center"><?php echo $volunteer->render();?></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <?php
            if(\Session::has('orgID')){
                $org_id = \Session::get('orgID');
            }
            else if(\Session::has('adminOrgID')){
                $org_id = \Session::get('adminOrgID');
            }
            $donations = \DB::table('donation_user')->where('org_id','=',$org_id)->join('b2b_user','b2b_user.b2b_user_id','=','donation_user.b2b_user_id')->get();
            $volunteers = \DB::table('volunteer_user')->join('b2b_user','b2b_user.b2b_user_id','=','volunteer_user.b2b_user_id')->where('org_id','=',$org_id)->get();
    ?>
    <div class="container">
        <h2 class="page-header heading">
            User Activity
        </h2>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center">Donations Received</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover table-bordered">
                            <th>Name</th>
                            <th>Scheme</th>
                            <th>Amount</th>
                            <th>Date of Donation</th>
                            @foreach($donations as $d)
                                <?php
                                $dname = \DB::table('donation')->where('donation_id','=',$d->donation_user_id)->pluck('scheme');
                                $dstart = \DB::table('donation')->where('donation_id','=',$d->donation_user_id)->pluck('created_at');
                                ?>
                                <tr>
                                    <td>
                                        {{$d->name}}
                                    </td>
                                    <td>
                                        {{$dname}}
                                    </td>
                                    <td>
                                        {{$d->amount}}
                                    </td>
                                    <td>
                                        {{$dstart}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center">Volunteers Joined</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover table-bordered">
                            <th>Name</th>
                            <th>Programme</th>
                            <th>Programme started on</th>
                            <th>Joined on</th>
                            @foreach($volunteers as $v)
                                <?php
                                $vname = \DB::table('volunteer')->where('volunteer_id','=',$v->volunteer_user_id)->pluck('title');
                                $vstart = \DB::table('volunteer')->where('volunteer_id','=',$v->volunteer_user_id)->pluck('created_at');
                            ?>
                                <tr>
                                    <td>
                                        {{$v->name}}
                                    </td>
                                    <td>
                                        {{$vname}}
                                    </td>
                                    <td>
                                        {{$vstart}}
                                    </td>
                                    <td>
                                        {{$v->joined_on}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection