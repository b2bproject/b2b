<?php
/**
 * Created by PhpStorm.
 * User: sajnaninisha
 * Date: 02-04-2016
 * Time: 18:23
 */
?>
@extends('user.header')

@section('content')
<div class="container" id="bodyheading">
    <h4 class="heading"><strong><span style="font-size: 140%; color: #3f45ad;">Edit Donation Scheme&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h4>
    @include('partials/flash')
    @include('errors.lists')

    <div class="container-fluid">
        @foreach($don as $d)
        <form action=../donation/{{$d->donation_id}}" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" data-toggle="validator">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <label for="scheme" class="control-label req">Scheme Title</label>
                            <div class="input-group col-md-11 col-xs-12">
                                <input type="text" class="form-control" id="title" name="title" value="@if(isset($d->scheme)){{$d->scheme}}@endif">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group">
                            <label for="status" class="control-label req">Activate this scheme</label>
                            <div class="input-group col-sm-12 col-xs-12" id="status">
                                <div class="col-sm-4 col-xs-6">
                                    <label for="yes" class="control-label">Yes</label>
                                    <input type="radio" name="status" id="yes" value="1" checked>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <label for="no" class="control-label">No</label>
                                    <input type="radio" name="status" id="no" value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="period" class="control-label req">Period</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="number" id="period" name="period" class="form-control" placeholder="Enter period in months" value="@if(isset($d->period)){{$d->period}}@endif"required>
                                <p class="help-block" style="font-size: 80%">Enter number of months and enter '0' for 1 time donation</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="amount" class="control-label req">Amount</label>
                            <div class="input-group col-xs-12">

                                <input type="number" id="amount" aria-describedby="sizing-addon3" class="form-control" name="amount" value="@if(isset($d->amount)){{$d->amount}}@endif"placeholder="Enter amount" aria-label="Amount (to the nearest Rupee)">
                                <p class="help-block" style="font-size: 80%">Amount needs to be Indian Rupees only</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-offset-2">
                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-primary btn-block" name="addNews">Edit Donation Scheme</button>
                            </div>
                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-danger btn-block" name="deletebtn" >Delete Donation Scheme</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
            @endforeach
    </div>
</div>
@endsection