<?php
/**
 * Created by PhpStorm.
 * User: sajnaninisha
 * Date: 02-04-2016
 * Time: 18:55
 */

?>
@extends('user.header')

@section('content')
<div class="container" id="bodyheading">
    <h4 class="heading"><strong><span style="font-size: 140%; color: #3f45ad;">Add Volunteering Programmes&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h4>

    <div class="container">
        @foreach($vol as $v)
        <form action="../volunteer/{{$v->volunteer_id}}" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" data-toggle="validator">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-sm-8 col-xs-12">
                        <div class="form-group">
                            <label for="title" class="control-label req">Programme Title</label>
                            <div class="input-group col-sm-12 col-xs-12">
                                <input type="text" class="form-control" id="title" name="title" value="@if(isset($v->title)){{$v->title}}@endif" placeholder="Enter Volunteering Programmes Title">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-offset-1 col-sm-3 col-xs-12" style="float: right">
                        <div class="form-group" style="float: right">
                            <label for="status" class="control-label req">Activate this programme</label>
                            <div class="input group col-sm-12 col-xs-12" id="status">
                                <div class="col-sm-4 col-xs-6">
                                    <label for="yes" class="control-label">Yes</label>
                                    <input type="radio" name="status" id="yes" value="1" checked>
                                </div>
                                <div class="col-sm-4 col-xs-6">
                                    <label for="no" class="control-label">No</label>
                                    <input type="radio" name="status" id="no" value="0">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="Description" class="control-label req">Description</label>
                            <div class="input-group col-sm-11 col-xs-12">
                                    <textarea name="description" id="description" cols="30" rows="10"
                                              class="form-control" placeholder="Describe your volunteer programme in 1000 characters" maxlength="1000">@if(isset($v->description)){{$v->description}}@endif</textarea>
                                <p class="help-block" style="font-size: 80%; float: right">Max character limit is 1000</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-offset-2">
                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-primary btn-block" name="addNews">Edit Donation Scheme</button>
                            </div>
                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-danger btn-block" name="deletebtn" >Delete Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
            @endforeach
    </div>
</div>
@endsection