<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 29-03-2016
 * Time: 12:53
 */
?>
@extends('user.header')

@section('content')

    <div class="container">
        <div class="row">
            <h2 class="heading page-header"><span style="font-size: 140%; color: #3f45ad;">Change/Add Logo</span></h2>
        </div>
        @include('partials.flash')
        @include('errors.lists')
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="help-block alert-info" style="padding: 10px"><strong>Changes once made to the logo cannot be revoked</strong></p>
                <form action="changeLogo" role="form" method="POST" enctype="multipart/form-data" data-toggle="validator">
                    <div class="row">
                    @if(\Session::has('logo') && \Session::get('logo')!="")
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <label for="oldLogo" class="label label-default" style="font-size: 110%">Current Logo</label>
                                <img src="../public/uploads/{{\Session::get('logo')}}" alt="Logo will appear here" class="img-responsive img-rounded" name="oldLogo" id="oldLogo" style="margin: 10px">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-8">
                                <div class="form-group">
                                    <label for="changeLogo">Change Logo</label>
                                    <input type="file" name="changelogo" id="changeLogo">
                                </div>
                            </div>
                        </div>
                    @elseif(\Session::get('logo')=="")
                        <div class="col-md-4 col-sm-4 col-xs-8">
                            <div class="form-group">
                                <label for="addLogo">Add Logo</label>
                                <input type="file" name="addlogo" id="addLogo">
                            </div>
                        </div>
                    @endif
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="form-group">
                                    <button style="margin-left:0px; margin-right: 20px" class="btn btn-primary btn-block" name="addImage" id="addImage">Make Changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection
