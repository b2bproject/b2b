<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 11-03-2016
 * Time: 00:00
 */
?>
@extends('user.header')
@section('content')
<div class="container" id="bodyheading">
    <h4 class="heading"><strong><span style="font-size: 140%; color: #3f45ad;">Edit Images&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h4>
    <p class="help-block">These images will appear in the slider</p>

    @include('partials/flash')
    @include('errors.lists')

    <div class="container">
        <form action="index/addImagesSubmit" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" data-toggle="validator">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image1" class="control-label req">Image 1</label>
                            <input type="file" name="image1" id="image1" required>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image2" class="control-label req">Image 2</label>
                            <input type="file" name="image2" id="image2" required>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image3" class="control-label">Image 3</label>
                            <input type="file" name="image3" id="image3">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image4" class="control-label">Image 4</label>
                            <input type="file" name="image4" id="image4">
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="image5" class="control-label">Image 5</label>
                            <input type="file" name="image5" id="image5">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-2">
                    <div class="col-sm-4 col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block" name="addImage">Add Images</button>
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <button type="reset" class="btn btn-default btn-block" name="resetImage">Reset form and fill again</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection