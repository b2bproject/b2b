<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 03-03-2016
 * Time: 22:58
 */
?>
    <div class="container" id="bodyheading">
        <h4 class="heading"><strong><span style="font-size: 140%; color: #3f45ad;">Add Product Details&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h4>
        @include('partials.flash')
        @include('errors.lists')
        <div class="container">
            <form class="form-horizontal" data-toggle="validator" role="form" method="post" action="../product/addProduct" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="product_name" class="control-label req">Product Name</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="text" id="product_name" placeholder="Enter product name" name="product_name" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="image" class="control-label req">Image</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="file" id="image" name="image">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="product_type_id" class="control-label req">Please select product type</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <select id="product_type_id" name="product_type_id" class="form-control">
                                    <option disabled>Please select product type</option>
                                    @foreach ($productType as $product => $product_type)
                                        <option value="{{ $product }}">{{ $product_type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cost" class="control-label req">Amount</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="number" id="cost" placeholder="Enter amount" class="form-control" name="cost" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-12">

                        <div class="form-group">
                            <label for="availability" class="control-label req">Availability</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="number" id="availability" placeholder="Enter quantity available" class="form-control" name="availability" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="control-label req">Description</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <textarea type="text" name="description" id="description" placeholder="Enter description" class="form-control" required rows="8"></textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-offset-2">
                            <div class="col-sm-4 col-xs-6">
                                <button type="submit" class="btn btn-primary btn-block" name="addProduct">Add Your Product</button>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-6">
                            <button type="reset" class="btn btn-default btn-block" name="clearform">Reset form and fill again</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>