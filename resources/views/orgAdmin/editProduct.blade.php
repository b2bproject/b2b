<?php
/**
 * Created by PhpStorm.
 * User: sajnaninisha
 * Date: 02-04-2016
 * Time: 16:59
 */

?>
@extends('user.header')

@section('content')
<div class="container" id="bodyheading">
    <h2 class=" page-header heading"><strong><span style="font-size: 140%; color: #3f45ad;">Edit Product Details&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h2>
    <div class="container">
        @foreach($product as $p)
        <form class="form-horizontal" data-toggle="validator" role="form" method="post" action="../product/{{$p->product_id}}" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="product_name" class="control-label req">Product Name</label>
                        <div class="input-group col-sm-10 col-xs-12">
                            <input type="text" id="product_name" placeholder="Enter product name" name="product_name" class="form-control" value="@if(isset($p->product_name)){{$p->product_name}}@endif" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="image" class="control-label req">Image</label>
                        <div class="input-group col-sm-10 col-xs-12">
                            <img src="../../../public/uploads/{{$p->image}}" alt="" class="img-responsive img-thumbnail" title="{{$p->image}}" style="width: 100px; height: 100px">
                            <input type="file" id="image" name="image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="product_type_id" class="control-label req">Please select product type</label>
                        <div class="input-group col-sm-10 col-xs-12">
                            <?php $c = $p->product_type_id;
                            //echo $c;?>
                            <?php  $type = \DB::table('product_type')->where('product_type_id', $c)->pluck('product_type'); ?>
                            <select id="product_type_id" name="product_type_id" class="form-control">
                                <option selected value="{{$p->product_type_id}}">{{$type}}</option>
                                <option disabled>Please select product type</option>
                                @foreach ($productType as $product => $product_type)
                                    <option value="{{ $product }}">{{ $product_type }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cost" class="control-label req">Amount</label>
                        <div class="input-group col-sm-10 col-xs-12">
                            <input type="number" id="cost" placeholder="Enter amount" class="form-control" name="cost" value="@if(isset($p->cost)){{$p->cost}}@endif" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xs-12">

                    <div class="form-group">
                        <label for="availability" class="control-label req">Availability</label>
                        <div class="input-group col-sm-10 col-xs-12">
                            <input type="number" id="availability" placeholder="Enter quantity available" class="form-control" name="availability" value="@if(isset($p->availability)){{$p->availability}}@endif"required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="control-label req">Description</label>
                        <div class="input-group col-sm-10 col-xs-12">
                            <textarea type="text" name="description" id="description" placeholder="Enter description" class="form-control" required rows="8">@if(isset($p->description)){{$p->description}}@endif</textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-offset-2">
                        <div class="col-sm-4 col-xs-6">
                            <button type="submit" class="btn btn-primary btn-block" name="addProduct">Edit Your Product</button>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                        <button type="submit" class="btn btn-danger btn-block" name="deletebtn" >Delete Product</button>
                    </div>
                </div>
            </div>
        </form>
            @endforeach
    </div>
</div>
@endsection