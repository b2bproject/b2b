<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 09-03-2016
 * Time: 22:51
 */
?>
@extends('user.header')

@section('content')

<div class="container" id="bodyheading">
    <h2 class="page-header heading"><strong><span style="font-size: 140%; color: #3f45ad;">Edit Project&nbsp;</span></strong><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h2>
    <div class="container">
        @foreach($project as $pro)
        <form action="../projects/{{$pro->project_id}}" class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" data-toggle="validator">
            <div class="row">
                <div class="col-lg-12">
                    <div class="col-sm-8 col-xs-12">
                        <div class="form-group">
                            <label for="title" class="control-label req">Title</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <input type="text" class="form-control" id="title" name="title" placeholder="Enter Project Title" value="@if(isset($pro->title)){{$pro->title}}@endif" required>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="project_id" value="{{$pro->project_id}}">
                    <div class="col-sm-4 col-xs-12">
                        <div class="form-group">
                            <label for="image" class="control-label req">Image</label>
                            <div class="input-group col-sm-10 col-xs-12">
                                <img src="../../../public/uploads/{{$pro->image}}" alt="" class="img-responsive img-thumbnail" title="{{$pro->image}}" style="width: 100px; height: 100px">
                                <input type="file" id="image" name="image">
                                 <p class="help-block" style="font-size: 80%">Image size must be less than 2mb</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-11 col-xs-12">
                        <div class="form-group">
                            <label for="description" class="control-label req">Description</label>
                            <div class="input-group col-xs-12">
                                    <textarea name="description" id="description" rows="8" placeholder="Enter Project Description in upto 1000 characters"
                                              class="form-control" required>@if(isset($pro->description)){{$pro->description}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-offset-2">
                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-primary btn-block" >Edit Project</button>
                            </div>

                            <div class="col-sm-4 col-xs-12" style="padding-bottom: 5px">
                                <button type="submit" class="btn btn-danger btn-block" name="deletebtn" >Delete Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
            @endforeach
    </div>
</div>
@endsection