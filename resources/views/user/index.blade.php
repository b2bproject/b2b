<?php ?>
@extends('user.header')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="background" id="bodyheading">
		  <div class="transbox">
			<div class="row" role="form" id="form-inline">
				<div class="col-md-12 col-sm-12 col-xs-12" id="inputdetails">
					<form action="search" method="get" role="form">
						<div class="input-group">
							<input type="text" class="form-control input-sm" placeholder="Search by name, location, product" name="search" id="search">
						<span class="input-group-btn">
							<button type="submit" class="btn btn-primary input-sm"><i class="glyphicon glyphicon-search"></i></button>
						</span>
						</div>
					</form>
				</div>
			</div>
		  </div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		@include('partials.flash')
		@include('errors.lists')
	</div>
</div>
<div class="container">

	<div class="row">
		<h1 class="page-header">
			ABOUT US
		</h1>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	</div>
	<div class="row">
		<h1 class="page-header">
			EVENTS AT TOP NGO(s)
		</h1>
		<ul class="bxslider">
			@foreach($events as $e)
				<li><a href="microwebsite/{{$e->org_id}}"><img src="public/uploads/{{$e->img}}" style="width:250px;height:250px" alt="Image"></a></li>
			@endforeach
		</ul>
	</div>

	<div class="row">
		<h1 class="page-header">
			FEATURED ORGANIZATIONS
		</h1>
		<div class="container dynamicTile">
			@foreach($forg as $org)
				<a href="microwebsite/{{$org->org_id}}" target="_blank">
					<div class="col-sm-2 col-xs-4">
						<div id="tile1" class="tile">
							<div class="carousel slide" data-ride="carousel">
							<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active text-center">
										<img src="public/uploads/{{$org->logo}}" style="position: absolute;margin: auto;top: 0;left: 0;height:100px;width:100px;right: 0;bottom: 0;" class="img-responsive" />
									</div>
									<div class="item text-center">
										<div class="icontext" style="text-align: center;">
											{{$org->name}}
										</div>
										<div class="icontext">
											<span class="glyphicon glyphicon-map-marker"></span>{{$org->city}}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			@endforeach
		</div>
	</div>
</div>
@include('user.footer')
@endsection