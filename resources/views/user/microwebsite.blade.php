<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 13-03-2016
 * Time: 14:41
 */
?>
@extends('user.header')
@section('content')

    <div class="container">
        <div class="panel panel-primary backgroundd" id="intro">
            <div class="panel-body" style="color:white;">
                @foreach($organisation as $org)
                    <div class="col-md-3 col-sm-6">
                        <img src="http://localhost/b2b/public/public/uploads/{{$org->logo}}" class="img-responsive" style="max-width: 200px;">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <h2>{{$org->name}}</h2>
                    </div>
                    <div class="col-md-offset-0 col-md-3">
                        <p class="orgdetails"><span class="glyphicon glyphicon-envelope">&nbsp;{{$org->primary_email}}</span><br>
                            <span class="glyphicon glyphicon-home">&nbsp;{{$org->address}}</span><br>
                            <span class="glyphicon glyphicon-earphone">&nbsp;{{$org->contact_no}}</span><br>
                            @if(($org->website) == '')
                                <span class="glyphicon glyphicon-earphone">&nbsp;{{$org->website}}</span>
                            @endif
                        </p>
                    </div>
                   <?php $id = $org->org_id;
                        $description = $org->description ?>
                @endforeach
            </div>

            <hr style="color:white;">
            <nav class="navbar navbar-default" style="border-color:#162252;">
                <ul class="nav navbar-nav1" style="background-color: #162252; ">
                    <li><a href="#section1">&nbsp;&nbsp;&nbsp;&nbsp;ABOUT US&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a href="#section2">&nbsp;&nbsp;&nbsp;&nbsp;VIEW PRODUCTS&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a href="#section3">&nbsp;&nbsp;&nbsp;&nbsp;PROJECTS/EVENTS&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a href="#section4">&nbsp;&nbsp;&nbsp;&nbsp;VOLUNTEER US&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                    <li><a href="#section4">&nbsp;&nbsp;&nbsp;&nbsp;DONATION&nbsp;&nbsp;&nbsp;&nbsp;</a></li>
                </ul>
            </nav>
        </div>
        @include('partials.flash')
        @include('errors.lists')
        <!--sections-->
        <div id="section1" >
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12" >
                        <h1 class="page-header heading head" style="text-align: center; color:blue" id="section1">ABOUT</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-6">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <?php $count=0 ?>
                                @foreach($images as $img)
                                    <div class="item <?php if($count==0){echo "active"; $count++;}?>">
                                        <img src="http://localhost/b2b/public/public/uploads/{{$img->img_name}}" alt="{{$img->img_name}}" style="height: 300px">
                                        <div class="carousel-caption">

                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-xs-12 col-md-6">
                        <p style="text-align:justify;">
                            {{$description}}
                        </p>
                    </div>
                </div>
        </div><!--/#section1-->

        <!--section2-->
        <div id="section2">
            <h1 class="page-header  heading head" style="text-align:center; color:blue" id="section2" >PRODUCTS</h1><br>
            <div class="row">
                @foreach($product as $pro)
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-default event">
                        <div class="panel-heading" id="products" >{{$pro->product_name}}</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <img class="img-responsive" src="http://localhost/b2b/public/public/uploads/{{$pro->image}}" style="border:2px solid #000080; margin: 5px; max-height: 100px">
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <p style="text-align:justify">{{$pro->description}} </p>
                                </div>
                            </div>
                            <center><a href="buyProduct/{{$pro->product_id}}" class="btn navbar-btn  btn-primary">BUY</a></center>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div><!--/#section2-->

        <!--section3-->
        <div id="section3" >
            <div class="container">
                <h1 class="page-header  heading head" style="text-align:center; color:blue" id="section3">EVENTS/ PROJECTS</h1>
                <br>
                <div class ="row">
                    <div class="col-md-6 col-sm-4 col-xs-4">
                        <h1 style="text-align:center; color:blue" id="headings"><center>EVENTS</center></h1>
                        <div class="row">
                            @foreach($event as $eve)
                            <div class="col-md-6 col-sm-12 col-xs-12 ">
                                <div class="panel panel-default event" >
                                    <div class="panel-heading" id="products">{{$eve->title}}</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <img class="img-responsive" src="http://localhost/b2b/public/public/uploads/{{$eve->img}}" style=" border:2px solid #eeeeee; margin:0 auto; max-height: 125px">
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p style="text-align:justify"><br>{{$eve->description}}</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <h1 style="text-align:center; color:blue" id="headings"><center>PROJECTS</center></h1>
                    <div class="col-md-6 col-sm-4 col-xs-4">
                        <div class="row">
                            @foreach($project as $pro)
                            <div class="col-md-6 col-sm-12 col-xs-12">

                                <div class="panel panel-default event" style="max-height: 325px">
                                    <div class="panel-heading" id="products">{{$pro->title}}</div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <img class="img-responsive" src="http://localhost/b2b/public/public/uploads/{{$pro->image}}" style="border:2px solid #021a40; margin: 0 auto; max-height: 125px">
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <p style="text-align:justify"><br>{{$pro->description}} </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/#section3-->
            <!--section4-->
            <h1 style="text-align:center; color:blue" id="headings"><center>VOLUNTEER/ DONATION</center></h1>
            <div id="section4">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class=" well">
                            <h1 class="page-header heading" style="text-align: center; color:blue" id="section4">VOLUNTEER</h1>
                            <h3>Volunteering is the best way to return what you get from the society to the society. It is to work for the needy without any greed and feel like being on top of this world</h3>
                            <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#volunteerModal">Join Us</button>
                        </div>
                    </div>
                    <!--/#section4-->

                    <!--section5-->
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="well">
                            <h1 class="page-header heading" style="text-align: center; color:blue" id="section5">DONATION</h1>
                            <h3>Donation is a medium to help others who can't afford to enjoy the basic human needs. It helps the poor to live their life with happiness. It helps the NGOs to make the most of their abilities  for helping the kind</h3>
                            <!-- Trigger the modal with a button -->
                            <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#donateModal">Donate</button>
                        </div>
                    </div>
                </div>
            </div>


    <!--/#section4-->
    <!-- Modal -->
    <div id="volunteerModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:blue">VOLUNTEERING TERMS</h1>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="container-fluid">
                            <form  class="form-horizontal" data-toggle="validator" role="form">
                                @foreach($volunteer as $v)
                                    <div class="row">
                                        <div class="col-md-6 col-sm-8 col-xs-12">
                                            <h3><strong>Volunteer Title: {{$v->title}}</strong></h3>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-8 col-xs-12">
                                            <strong><h4>Volunteer Description:</h4></strong>&nbsp;{{$v->description}}
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-8">
                                            <a href="joinvolunteering/{{$v->volunteer_id}}"><button type="button" class="btn btn-primary btn-block">Volunteer</button></a>
                                        </div>
                                    </div>
                                @endforeach
                            </form>
                        </div>
                    </div><!--/.container-->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="donateModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:blue">DONATION</h1>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form  class="form-horizontal" data-toggle="validator" role="form">
                            @foreach($donation as $d)
                                <div class="row">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <h3><strong>Donation Scheme: {{$d->scheme}}</strong></h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <strong><h4>Donation Amount:</h4></strong>&nbsp;{{$d->amount}}
                                    </div>
                                    <div class="col-md-6 col-sm-8 col-xs-12">
                                        <strong><h4>Donation Period:</h4></strong>&nbsp;{{$d->period}}
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-8">
                                        <a href="donate/{{$d->donation_id}}"><button type="button" class="btn btn-primary btn-block">Donate</button></a>
                                    </div>
                                </div>
                            @endforeach
                        </form>
                    </div><!--/.container-->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection


