<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 14-04-2016
 * Time: 23:37
 */
?>
@extends('user.header')
@section('content')
    <div class="container">
        <div class="row">
            @if(\Session::has('userStatus'))
                <h1 class="page-header heading">
                    Welcome, {{\Session::get('userName')}}!
                </h1>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModalEdit">
                    Edit Personal Details
                </button>
            @endif
            @include('partials.flash')
            @include('errors.lists')
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                @if($volunteer->count() > 0)
                    <h3 class="page-header">
                        Volunteering Programmes
                    </h3>
                    <?php
                        $vol = $volunteer->get();
                    ?>
                <ul>
                    @foreach($vol as $v)
                        <?php
                            $ngo = \DB::table('organisation')->where('org_id','=',$v->org_id)->pluck('name');
                        ?>
                        <li>
                            <h4>
                                <strong>{{$v->title}}</strong><br>
                                <strong>Joined On: </strong>{{$v->joined_on}}<br>
                                <a href="microwebsite/{{$v->org_id}}"><strong>NGO: </strong>{{$ngo}}</a>
                            </h4>
                        </li>
                            <hr>
                    @endforeach
                </ul>
                @endif
            </div>
            <div class="col-md-4">
                @if($donation->count() > 0)
                    <h3 class="page-header">
                        Donations
                    </h3>
                    <?php
                        $don = $donation->get();
                    ?>
                    <ul>
                        @foreach($don as $d)
                            <?php
                            $ngo = \DB::table('organisation')->where('org_id','=',$d->org_id)->pluck('name');
                            ?>
                            <li>
                                <h4>
                                    <strong>{{$d->scheme}}</strong><br>
                                    <strong>Donated On: </strong>{{$d->donated_on}}<br>
                                    <strong>Amount </strong>{{$d->amount}}<br>
                                    <a href="microwebsite/{{$d->org_id}}"><strong>NGO: </strong>{{$ngo}}</a>
                                </h4>
                            </li>
                                <hr>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="col-md-4">
                @if($product->count() > 0)
                    <h3 class="page-header">
                        Purchased Products
                    </h3>
                    <?php
                      $pro = $product->get();
                    ?>
                    <ul>
                        @foreach($pro as $p)
                            <?php
                            $ngo = \DB::table('organisation')->where('org_id','=',$d->org_id)->pluck('name');
                            ?>
                            <li>
                                <div class="row">
                                <div class="col-md-4">
                                    <img src="../public/uploads/{{$p->image}}" alt="Product Image" class="img-responsive" title="Product Logo">
                                </div>
                                <h4>
                                    <strong>{{$p->product_name}}</strong><br>
                                    <strong>Cost: </strong>{{$p->cost}}<br>
                                </h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <strong>Purchase Date: </strong>{{$p->date}}<br>
                                        <strong>NGO: </strong><a href="microwebsite/{{$d->org_id}}">{{$ngo}}</a>
                                    </div>
                                </div>
                                <hr>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="edit" role="form" method="POST">
                    @foreach($user as $u)
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Personal Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" value="{{$u->name}}" name="name" placeholder="Enter your name" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Enter your email" value="{{$u->email}}" id="email" disabled>
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile No</label>
                                <input type="number" class="form-control" name="mobile" id="mobile" value="{{$u->mobile_no}}" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Make Changes</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <p class="help-block">Changes are irrevokable</p>
                        </div>
                    @endforeach
                </form>
            </div>
        </div>
    </div>

@endsection
