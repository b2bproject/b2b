<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>
			<?php if(\Session::has('page')){
					echo \Session::get('page');
				}
				else{
					echo "B2B Management and Lead Generation";
				}
			?>
		</title>
		<link rel='shortcut icon' href='http://localhost/b2b/public/images/favicon.ico' type='image/x-icon'/ >
		<link href="http://localhost/b2b/public/asset/css/bootstrap.min.css" rel="stylesheet">
		<link href="http://localhost/b2b/public/asset/css/bootstrap-theme.min.css" rel="stylesheet" >
		<link href="http://localhost/b2b/public/asset/css/style1.css" rel="stylesheet" >
		<link href="http://localhost/b2b/public/asset/css/jquery.bxslider.css" rel="stylesheet" >
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<script>
			$(function() {
				$("#datepicker").datepicker();
			});
		</script>
		<script>
			$(function() {
				$("#datepicker2").datepicker();
			});
		</script>
	</head>
	<nav class="navbar navbar-default navbar-fixed-top" style="font-family: sans; font-size: 110%">
		<div class="container-fluid" id="header">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="true">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<p><a class="navbar-brand" href="http://localhost/b2b/public" style="color:#fff;">B2B Management and Lead Generation</a></p>
			</div><!-- End navbar-header-->
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav navbar-right " style="padding:20px 0px 0px 0px;">
					<li><a href="http://localhost/b2b/public" title="Home">Home</a></li>
					@if(\Session::has('userStatus') && (\Session::get('userStatus')==1) || \Session::get('userStatus')=='2')
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NGO Account<span class="caret"></span></a>
							<ul class="dropdown-menu" style="background-color: #6161ff">
								<?php
										if(\Session::get('userStatus')=='1'){
											$org_id = \Session::get('orgID');
										}
										else if(\Session::get('userStatus')=='2'){
											$org_id = \Session::get('admindOrgID');
										}
									?>
								<li><a href="http://localhost/b2b/public/orgAdmin/index/{{$org_id}}" title="Manage your NGO's account from here">NGO Account</a></li>
								<li><a href="http://localhost/b2b/public/orgAdmin/statistics" title="View graphical representation of user activities">Statistics</a></li>
								<li><a href="http://localhost/b2b/public/orgAdmin/editngo" title="View graphical representation of user activities">Edit Details</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Edit Images<span class="caret"></span></a>
							<ul class="dropdown-menu" style="background-color: #6161ff">
								<li><a href="http://localhost/b2b/public/orgAdmin/sliderImages">Change Slider Images</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="http://localhost/b2b/public/orgAdmin/logo">Change Logo</a></li>
							</ul>
						</li>
					@endif
					<li><a href="http://localhost/b2b/public/browseAll" title="Browse all NGOs in our database">Browse NGOs</a></li>
					<li><a href="http://localhost/b2b/public/contactAdmin" title="Contact Us">Contact Us</a></li>
					<li><a href="http://localhost/b2b/public/about" title="Know More About Us">About Us</a></li>
					@if(!\Session::has('userSession'))
						<li><a href="#myModal" data-toggle="modal" data-target="#myModal">Login</a></li>
						<li><a href="#myModalRegister" data-toggle="modal" data-target="#myModalRegister"><strong>SignUp</strong></a></li>
					@elseif(\Session::has('userSession'))
						@if(\Session::has('userStatus') && (\Session::get('userStatus')==0) || \Session::get('userStatus')==2)
							<li><a href="http://localhost/b2b/public/user/addNGO" title="Add your NGO">Add Organisation</a></li>
						@endif
					@endif
					@if(\Session::has('userSession'))
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{\Session::get('userName')}}<span class="caret"></span></a>
							<ul class="dropdown-menu" style="background-color: #6161ff">
								<li><a href="http://localhost/b2b/public/user/myAccount" title="Manage Personal Account">Personal Profile</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="http://localhost/b2b/public/user/logout" title="Logout">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>

			</div>
			<!-- End container-->
		</div><!-- End navbar-->
    </nav>
	<div class="modal fade" id="myModal">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Log-in</h4>
        </div>
        <div class="modal-body">
		<form role="form" method="POST" action="http://localhost/b2b/public/user/login">
          <div class="form-group">
    		<label for="exampleInputEmail1">Email address</label>
    		<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                <input type="email" class="form-control" id="inputEmail3" placeholder="Email" required name="email">
            </div>
  		  </div>
		  <div class="form-group">
		  	<label for="exampleInputPassword1">Password</label>
			<div class="input-group">
                <span class="input-group-addon"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
				<input type="password" class="form-control" id="inputPassword3" placeholder="Password" required name="password">
            </div>
		  </div>
		   <div class="form-group">
                <button type="submit" class="btn btn-success btn-md" name="login"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Sign in</button>
                <button type="reset" class="btn btn-primary btn-md" name="reset"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Reset</button>
           </div>
        </form>
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="myModalRegister">
	<div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Register with Us!</h4>
        </div>
        <div class="modal-body">
			<form role="form" method="POST" action="http://localhost/b2b/public/user/register">
				<div class="form-group ">
					<label for="message" class="control-label">Full Name</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
						<input type="text" class="form-control" id="inputEmail3" placeholder="Full Name" required name="name">
					</div>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Email address</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
						<input type="email" class="form-control" id="inputEmail3" placeholder="Email" required name="email">
					</div>
				</div>
				<div class="form-group">
					<label for="mobile">Mobile Number</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
						<input type="text" class="form-control" id="mobile" placeholder="Mobile Number" required name="mobile" maxlength="10">
					</div>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
						<input type="password" class="form-control" id="inputPassword3" placeholder="Password" required name="password">
					</div>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Confirm Password</label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
						<input type="password" class="form-control" id="inputPassword3" placeholder="Confirm Password" required name="password_confirmation">
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-success btn-md" name="register">Register</button>
				</div>
			</form>
        </div>          
      </div>
    </div>
</div>
	<body style="font-family: Georgia SansSerif Times; font-size: 1.4em ">
	@yield('content')

	</body>
	<div class="container-fluid footer-bg" style="font-size:17px;padding-top: 10px; margin-top: 10px">
		<div class="row">
			<div class="col-md-offset-2 col-md-2 text-center" style="color:#fff;">
				<h4><a href="http://localhost/b2b/public/terms" style="color: #fff;">Terms & conditions</a></h4>
				<h4><a href="http://localhost/b2b/public/privacy" style="color: #fff;">Privacy Policy</a></h4>
				<h4><a href="http://localhost/b2b/public/search" style="color: #fff;">Search NGOs</a></h4>
			</div>
			<div class="col-md-offset-1 col-md-2  text-center" style="color:white;">
				<h4><a href="http://localhost/b2b/public/about" style="color: #fff;">About Us</a></h4>
				<h4><a href="http://localhost/b2b/public/contactAdmin" style="color: #fff;">Contact Us</a></h4>
				<h4><a href="http://localhost/b2b/public/browseAll" style="color: #fff;">Top Visited NGOs</a></h4>
			</div>
			<div class="col-md-offset-1 col-md-3  text-center" style="color:white;">
				Connect With Us Through
				<div class=" pull-center">
					<a href="https://www.facebook.com/groups/vsmandal.dombivli" target="_blank" style="color:#fff"><button class="btn btn-facebook btn-primary" ><i class="fa fa-facebook fa-3px"></i> </button></a>
					<a href="https://twitter.com/vsmandal" target="_blank" style="color: #fff;"><button class="btn btn-twitter" style="background-color:#55ACEE" ><i class="fa fa-twitter fa-3px"></i> </button></a>
					<a href="https://www.linkedin.com/in/thadaninilesh" style="color: #fff" target="_blank"><button class="btn btn-linkedin" style="background-color:#007BB6"><i class="fa fa-linkedin fa-3px"></i></button></a>
					<a href="https://www.google.com/+VsmandalDombivli" target="_blank" style="color: #fff;"><button class="btn btn-google-plus btn-danger"><i class="fa fa-google-plus fa-3px"></i></button></a>
				</div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-md-6">
				<p style="color:white;font-size:17px;">Copyright &copy; 2016. All Rights Reserved. <a href="" style="color:#fff">NGOConnect.com</a></p>
			</div>
			<div class="col-md-6">
				<p style="color:white;font-size:17px;">We feel happy to make you happy :)</p>
			</div>


		</div>
	</div>
	</footer>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="http://localhost/b2b/public/asset/js/bootstrap.min.js"></script>
	<script src="http://localhost/b2b/public/asset/js/jquery.bxslider.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			// navigation click actions
			$('.scroll-link').on('click', function(event){
				event.preventDefault();
				var sectionID = $(this).attr("data-id");
				scrollToID('#' + sectionID, 1500);
			});
			// scroll to top action
			$('.scroll-top').on('click', function(event) {
				event.preventDefault();
				$('html, body').animate({scrollTop:0}, 'slow');
			});
			// mobile nav toggle
			$('#nav-toggle').on('click', function (event) {
				event.preventDefault();
				$('#main-nav').toggleClass("open");
			});
		});
		// scroll function
		function scrollToID(id, speed){
			var offSet = 50;
			var targetOffset = $(id).offset().top - offSet;
			var mainNav = $('#main-nav');
			$('html,body').animate({scrollTop:targetOffset}, speed);
			if (mainNav.hasClass("open")) {
				mainNav.css("height", "1px").removeClass("in").addClass("collapse");
				mainNav.removeClass("open");
			}
		}
		if (typeof console === "undefined") {
			console = {
				log: function() { }
			};
		}
	</script>
</html>