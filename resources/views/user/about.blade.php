<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 15-04-2016
 * Time: 13:42
 */
?>
@extends('user.header')
@section('content')
    <div class="container" style="margin-bottom: 10px">
        <div class="row">
            <h1 class="page-header heading">
                About Us!
            </h1>
        </div>
        @include('partials.flash')
        @include('errors.lists')


        <div class="row">
            <div class="col-md-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aspernatur consequuntur dolor doloribus excepturi ipsam nam neque quasi! Consectetur dignissimos fuga harum id illum in laudantium neque reiciendis veritatis voluptatibus!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur minus molestiae nemo quis sapiente sit. Dolore dolores fugit ipsam minus mollitia nam nihil nostrum quaerat quis, rem tempore ut velit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda cupiditate magnam nemo nisi odit optio, sint temporibus! Debitis doloremque iste quo sequi, similique suscipit ullam voluptatem? Deleniti enim, neque!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur dicta enim itaque voluptatum. Architecto aspernatur, dolor error fugit rerum veniam? Animi cumque est excepturi, labore maiores ratione voluptas voluptates voluptatum.</p>
            </div>
            <div class="col-md-4">
                <img src="images/about1.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-md-4">
                <img src="images/about2.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-md-8">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aspernatur consequuntur dolor doloribus excepturi ipsam nam neque quasi! Consectetur dignissimos fuga harum id illum in laudantium neque reiciendis veritatis voluptatibus!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur minus molestiae nemo quis sapiente sit. Dolore dolores fugit ipsam minus mollitia nam nihil nostrum quaerat quis, rem tempore ut velit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus assumenda cupiditate magnam nemo nisi odit optio, sint temporibus! Debitis doloremque iste quo sequi, similique suscipit ullam voluptatem? Deleniti enim, neque!</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur dicta enim itaque voluptatum. Architecto aspernatur, dolor error fugit rerum veniam? Animi cumque est excepturi, labore maiores ratione voluptas voluptates voluptatum.</p>
            </div>
        </div>
    </div>
@endsection