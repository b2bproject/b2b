<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 16-04-2016
 * Time: 07:35
 */
?>
@extends('user.header')
@section('content')
    <div class="container">
        <div class="row">
            <h1 class="page-header heading">
                Buy Product <p class="help-block">(Please fill out the form below)</p>
            </h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach($product as $pro)
                    <form action="confirmPurchase" method="POST" role="form">
                        <div class="col-md-8">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4><label for="email">Email: {{\Session::get('userSession')}}</label></h4>
                                    <input type="email" class="form-control" name="username" disabled value="{{\Session::get('userSession')}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4><label for="pname">Product Name: </label></h4>
                                    <input type="text" class="form-control" name="pname" disabled value="{{$pro->product_name}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4><label for="pcost">Product Cost:</label></h4>
                                    <input type="text" class="form-control" name="pcost" disabled value="{{$pro->cost}}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?php
                                        $ngo = \DB::table('organisation')->select('name')->where('org_id','=',$pro->org_id)->pluck('name');
                                    ?>
                                    <h4><label for="pngo">Product NGO: {{$ngo}}</label></h4>
                                        <input type="text" class="form-control" name="ngo" disabled value="{{$ngo}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="pimage">Product Image: </label>
                            <img src="../public/uploads/{{$pro->image}}" alt="Product Image" class="img-responsive img-thumbnail">
                        </div>
                        <div class="col-md-12">
                            <label for="address">Address:</label>
                            <textarea name="address" id="address" class="form-control" placeholder="Enter your address" required></textarea>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px">
                            <div class="col-sm-offset-3 col-sm-6 col-xs-12">
                                <button class="btn btn-primary btn-block btn-sm" name="confirmPurchase" type="submit">
                                    Confirm Purchase
                                </button>
                                <p class="help-block">(All purchases are under Cash On Delivery)</p>
                            </div>
                        </div>
                        <input type="hidden" value="{{$pro->org_id}}" name="org_id">
                        <input type="hidden" value="{{$pro->product_id}}" name="pid">
                    </form>
                @endforeach
            </div>
        </div>
    </div>
