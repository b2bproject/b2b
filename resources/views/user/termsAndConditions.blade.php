<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 15-04-2016
 * Time: 13:56
 */
?>
@extends('user.header')
@section('content')
    <div class="container">
        <div class="row">
            <h1 class="page-header heading">
                Terms and Conditions
            </h1>
        </div>
        @include('partials.flash')
        @include('errors.lists')

        <div class="row">
            <div class="col-md-10">
                <ol style="font-size: 1.2em">
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis iste neque nulla omnis quaerat? Autem cumque dolore, doloremque ea eveniet laboriosam mollitia nihil, placeat porro, quasi quod repellat saepe voluptatibus!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis iste neque nulla omnis quaerat? Autem cumque dolore, doloremque ea eveniet laboriosam mollitia nihil, placeat porro, quasi quod repellat saepe voluptatibus!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi cupiditate error exercitationem facilis illum ipsum iure iusto molestiae nihil omnis, porro quasi quo, reiciendis repellendus sapiente sequi vel voluptatibus?</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis iste neque nulla omnis quaerat? Autem cumque dolore, doloremque ea eveniet laboriosam mollitia nihil, placeat porro, quasi quod repellat saepe voluptatibus!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi cupiditate error exercitationem facilis illum ipsum iure iusto molestiae nihil omnis, porro quasi quo, reiciendis repellendus sapiente sequi vel voluptatibus?</li><li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis iste neque nulla omnis quaerat? Autem cumque dolore, doloremque ea eveniet laboriosam mollitia nihil, placeat porro, quasi quod repellat saepe voluptatibus!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi cupiditate error exercitationem facilis illum ipsum iure iusto molestiae nihil omnis, porro quasi quo, reiciendis repellendus sapiente sequi vel voluptatibus?</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi cupiditate error exercitationem facilis illum ipsum iure iusto molestiae nihil omnis, porro quasi quo, reiciendis repellendus sapiente sequi vel voluptatibus?</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi cupiditate error exercitationem facilis illum ipsum iure iusto molestiae nihil omnis, porro quasi quo, reiciendis repellendus sapiente sequi vel voluptatibus?</li>
                </ol>
            </div>
            <div class="col-md-2" style="text-align: center">
                <img src="images/tnc1.jpg" alt="" class="img-responsive img-thumbnail">
            </div>
        </div>


    </div>
@endsection
