<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 15-04-2016
 * Time: 14:28
 */
?>
@extends('user.header')
@section('content')
    <div class="container">
        <div class="row">
            <h1 class="page-header heading">
                Privacy Policy
            </h1>
        </div>
        @include('partials.flash')
        @include('errors.lists')

        <div class="row" style="font-size: 1.1em">
            <div class="col-md-10">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequatur libero quas vitae! At autem cupiditate fugiat in, ipsa minus nobis perspiciatis quae quasi quo tempora vitae, voluptas voluptate! Maxime.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolore ex illum in minus perferendis suscipit! Accusantium corporis, eius ex exercitationem illum itaque iure necessitatibus praesentium quae quis sint velit! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio ex fugiat nulla, quos rerum voluptates? Culpa iste laudantium quia sequi suscipit. At deleniti, dignissimos ea expedita in quibusdam sint voluptas!</p>
            </div>
            <div class="col-md-2">
                <img src="images/pp2.jpg" alt="Privacy Polic" class="img-responsive img-thumbnail">
            </div>
            <div class="col-md-4" style="text-align: center">
                <img src="images/pp1.jpg" alt="Privacy Policy" class="img-responsive img-thumbnail">
            </div>
            <div class="col-md-8">
                <ol>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis iste neque nulla omnis quaerat? Autem cumque dolore, doloremque ea eveniet laboriosam mollitia nihil, placeat porro, quasi quod repellat saepe voluptatibus!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis iste neque nulla omnis quaerat? Autem cumque dolore, doloremque ea eveniet laboriosam mollitia nihil, placeat porro, quasi quod repellat saepe voluptatibus!</li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi cupiditate error exercitationem facilis illum ipsum iure iusto molestiae nihil omnis, porro quasi quo, reiciendis repellendus sapiente sequi vel voluptatibus?</li>
                </ol>
            </div>
        </div>


    </div>
@endsection
