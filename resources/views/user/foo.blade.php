<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 13-03-2016
 * Time: 14:56
 */
?>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="http://localhost/b2b/public/asset/js/bootstrap.min.js"></script>
<script src="http://localhost/b2b/public/asset/js/jquery.bxslider.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        // navigation click actions
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var sectionID = $(this).attr("data-id");
            scrollToID('#' + sectionID, 1500);
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop:0}, 'slow');
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 50;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
</script>
<script>
    $('#myModal').on('shown.bs.modal', function ()
    { $('#myInput').focus()})
</script>
<script>
    $(document).on("click",".b2l",function(){
        $('#myModalRegister').modal('hide');
        $('#myModal').modal('show');
    });
</script>
<script>
    $(document).on("click",".reg",function(){
        $('#myModalRegister').modal('show');
        $('#myModal').modal('hide');
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".tile").height($("#tile1").width());
        $(".carousel").height($("#tile1").width());
        $(".item").height($("#tile1").width());

        $(window).resize(function () {
            if (this.resizeTO) clearTimeout(this.resizeTO);
            this.resizeTO = setTimeout(function () {
                $(this).trigger('resizeEnd');
            }, 10);
        });

        $(window).bind('resizeEnd', function () {
            $(".tile").height($("#tile1").width());
            $(".carousel").height($("#tile1").width());
            $(".item").height($("#tile1").width());
        });
    });
</script>
<script>$('.bxslider').bxSlider({
        minSlides: 3,
        maxSlides: 4,
        slideWidth: 300,
        slideMargin: 2

    });</script>
</html>
