<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 15-04-2016
 * Time: 01:33
 */
?>
@extends('user.header')
@section('content')
    <div class="container">
        <div class="row">
            <h1 style="color:#000080;" class="page-header">TOP VISITED ORGANIZATIONS</h1>
        </div>
        <div class="row">
            @foreach($ngoList as $ngo)
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="panel panel-primary" style="height: 100%">
                        <a href="microwebsite/{{$ngo->org_id}}">
                            <div class="panel-heading" id="products" style="height: 80px">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4 col-xs-6">
                                        <img src="public/uploads/{{$ngo->logo}}" class="img-responsive" style="height:60px;" />
                                    </div>
                                    <div class="col-md-9 col-sm-8 col-xs-6">
                                        <h4 style="font-weight:bold;color:white" class="text-center;">{{$ngo->name}}</h4>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="panel-body" style="min-height: 200px">
                            <p class="text-justify">{{$ngo->description}}</p>
                        </div>
                        <div class="panel-footer" style="max-height: 150px">
                            <div class="row">
                                <div class="icontext text-center col-md-6 col-sm-6 col-xs-6">
                                    <span class="glyphicon glyphicon-map-marker" style="color:#000080;font-size: 80%"></span>
                                    <p style="color:#000080;font-size:50%">{{$ngo->address}}</p>
                                </div>
                                <div class="icontext text-center col-md-6 col-sm-6 col-xs-6">
                                    <span class="glyphicon glyphicon-earphone" style="color:#000080;font-size: 80%"></span>
                                    <p style="color:#000080;font-size:50%">{{$ngo->contact_no}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="container">
            <div class="row">
                <h4><a href="browseMore" class="btn btn-info">View More NGOs</a></h4>
            </div>
        </div>
    </div>
@endsection
