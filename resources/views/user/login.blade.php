<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 15-04-2016
 * Time: 23:53
 */
?>
@extends('user.header')
@section('content')
    <div class="container">
        <div class="row">
            <h1 class="page-header">
                Login first
            </h1>
            @include('partials.flash')
            @include('errors.lists')
            <div class="col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
                <form action="login" method="POST" role="form">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Login
                            </h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email" required name="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password" required name="password">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-md" name="login"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Sign in</button>
                                <button type="reset" class="btn btn-primary btn-md" name="reset"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Reset</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
