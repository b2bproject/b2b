<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 09-09-2015
 * Time: 16:26
 */
?>


@if(Session::has('flash_message'))
    <div class="row" style="margin-top: 25px">
        <div class="alert alert-success alert-dismissable" {{ Session::has('flash_message_important') ? 'alert-important' : ''}}>
            @if(Session::has('flash_message_important'))
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
            {{ Session::get('flash_message') }}
        </div>
    </div>
@endif
@if(Session::has('error_message'))
    <div class="row" style="margin-top: 25px">
        <div class="alert alert-danger alert-dismissable" {{ Session::has('error_message_important') ? 'alert-important' : ''}}>
            @if(Session::has('error_message_important'))
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
            {{ Session::get('error_message') }}
        </div>
    </div>
@endif
@if(Session::has('info_message'))
    <div class="row" style="margin-top: 25px">
        <div class="alert alert-info alert-dismissable" {{ Session::has('info_message_important') ? 'alert-important' : ''}}>
            @if(Session::has('info_message_important'))
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
            {{ Session::get('info_message') }}
        </div>
    </div>
@endif
@if(Session::has('warning_message'))
    <div class="row" style="margin-top: 25px">
        <div class="alert alert-warning alert-dismissable" {{ Session::has('warning_message_important') ? 'alert-important' : ''}}>
            @if(Session::has('warning_message_important'))
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
            {{ Session::get('warning_message') }}
        </div>
    </div>
@endif
