@extends('user.header')

@section('content')
<div class="container-fluid">
    <h1  style="color:blue;">CONTACT DETAILS!</h1><br>
    @include('partials/flash')
    @include('errors.lists')

    <form  class="form-horizontal" data-toggle="validator" role="form" action="http://localhost/b2b/public/contact/sendMessage" method="post">

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group ">
                <label for="name" class="col-md-2 col-sm-2 col-xs-3 control-label"> Name</label>
                <div class="col-md-10 col-sm-10 col-xs-9 input-group">
                    <input type="text" id="name" placeholder="Your name"  class="form-control" name="name" required>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-md-2 col-sm-2 col-xs-3 control-label">Email</label>
                <div class="col-md-10 col-md-10 col-sm-10 col-xs-9 input-group">
                    <input type="email" id="email1" placeholder="Your email" class="form-control" name="email" required>
                </div>
            </div>
            <div class="form-group">
                <label for="contact_no" class="col-md-2  col-sm-2 col-xs-3 control-label"> Contact No.</label>
                <div class="col-md-10 col-md-10 col-sm-10 col-xs-9 input-group">
                    <input type="text" id="contact_no" placeholder="Your contact no." class="form-control" name="contact_no" required>
                </div>
            </div>
            <div class="form-group">
                <label for="work" class="col-md-2 col-sm-2 col-xs-3 control-label"> Message</label>
                <div class="col-md-10 col-md-10 col-sm-10 col-xs-9 input-group" >
                    <textarea type="text" id="message" placeholder="Your message" class="form-control" name="msg"  required></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-2 col-xs-10 col-xs-offset-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>


    </form>
</div>
@endsection
