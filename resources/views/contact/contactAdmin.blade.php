<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 01-03-2016
 * Time: 09:44
 */
?>
@extends('user.header')
@section('content')
    <div class="container" style="margin-bottom: 10px">
        @include('partials/flash')
        @include('errors.lists')

            <div class="col-md-8 col-sm-8 col-xs-12" style="border-right: 1px solid #eeeeee">
                <div class="row">
                    <h1 class="page-header heading"><span style="color: #234fff">Contact Us!</span></h1>
                </div>
                <form  class="form-horizontal" data-toggle="validator" role="form" method="POST" action="contact/sendFeedbackToAdmin">
                    <div class="container-fluid">
                        <div class="form-group">
                            <label for="name" class="control-label"> Name</label>
                            <input type="text" id="name" placeholder="Enter your fullname" name="name" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                                <input type="email" id="email1" placeholder="Enter your E-mail Address for any correspondence" name="email" class="form-control" required>
                        </div>


                        <div class="form-group">
                            <label for="contact_no" class="control-label"> Contact No.</label>
                                <input type="number" id="contact_no" placeholder="Enter your contact number" name="contact_no" class="form-control" required>
                        </div>


                        <div class="form-group">
                            <label for="work" class="control-label"> Message</label>

                                <textarea type="text" id="message" placeholder="Enter your detailed message description in about less than 1000 words" class="form-control" name="msg" required></textarea>

                        </div>
                        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-2 col-xs-10 col-xs-offset-3">
                            <button type="submit" class="btn btn-primary btn-block" name="submit">Send Details!</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <h1 class="page-header heading">Address</h1>
                <i class="glyphicon glyphicon-map-marker">&nbsp;Somewhere in the cloud, maybe on the seventh heaven</i>
                <hr>
                <i class="glyphicon glyphicon-envelope">&nbsp;thadaninilesh(at)gmail(dot)com</i>
                <hr>
                <i class="glyphicon glyphicon-phone">&nbsp;(+91)-9372111555</i>
                <hr>
                <a href="https://www.google.co.in/maps/place/vesit/@19.0493198,72.88753,20z/data=!4m2!3m1!1s0x3be7c8afcfc0233d:0x7523efd59fa9f61e?hl=en" target="_blank"><i class="glyphicon glyphicon-globe"></i>&nbsp;Locate us on map</a>
                <hr>
                <a href="#">Facebook</a>&nbsp;<a href="">LinkedIn</a>
            </div>
        </div>

@endsection
