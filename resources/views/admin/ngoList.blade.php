<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 07-03-2016
 * Time: 18:13
 */
?>

    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <h1 class="page-header heading">
                    Registered NGO(s)
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="text-align: center;">Complete NGO List</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover table-bordered">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Contact No</th>
                            <th>Registration date</th>
                            <th>Active Status</th>
                            <th>Edit</th>
                            @foreach($ngoList as $ngo)
                                <tr>
                                    <td><a href="../admin/editngo/{{$ngo->org_id}}">{{$ngo->org_id}}</a></td>
                                    <td>{{$ngo->name}}</td>
                                    <td>{{$ngo->address}}</td>
                                    <td>{{$ngo->contact_no}}</td>
                                    <td>{{$ngo->created_at}}</td>
                                    <td>{{$ngo->status=='0'?'Inactive':'Inactive'}}</td>
                                    <td><a href="../admin/editngo/{{$ngo->org_id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="panel-footer"><span style="alignment: center"><?php echo $ngoList->render();?></span></div>
                </div>
            </div>
        </div>
    </div>