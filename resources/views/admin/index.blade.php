<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 11-04-2016
 * Time: 22:06
 */
?>
@extends('user.header')
@section('content')
    <div class="container">
        <div class="row">
            <h1 class="page-header heading">
                Welcome Site Admin
            </h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @include('partials.flash')
            @include('errors.lists')
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-3">
                <div class="btn-group-vertical" role="group">
                    <a href="viewAll" style="text-decoration: none"><button class="btn btn-default btn-block" name="ngoList">View All NGOs</button></a>
                    <hr>
                    <a href="topNGOs" style="text-decoration: none"><button class="btn btn-info btn-block" name="topNGOs">View Top NGOs</button></a>
                    <hr>
                    <a href="featuredNGOs" style="text-decoration: none"><button class="btn btn-success btn-block" name="featuredNGOs">View Featured NGOs</button></a>
                </div>
            </div>
            <div class="col-md-10">
                @if(\Session::get('contentType')=='allNGO')
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title" style="text-align: center;">Complete NGO List</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover table-bordered">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact No</th>
                                <th>Registration date</th>
                                <th>Active Status</th>
                                <th>View Microwebsite</th>
                                <th>Edit</th>
                                <th>Make Featured</th>
                                <form action="makeFeatured" method="POST">
                                    @foreach($ngoList as $ngo)
                                        <tr style="text-align: center">
                                            <td><a href="../admin/editngo/{{$ngo->org_id}}">{{$ngo->org_id}}</a></td>
                                            <td>{{$ngo->name}}</td>
                                            <td>{{$ngo->address}}</td>
                                            <td>{{$ngo->contact_no}}</td>
                                            <td>{{$ngo->created_at}}</td>
                                            <td>{{$ngo->status=='0'?'Inactive':'Inactive'}}</td>
                                            <td><a href="../microwebsite/{{$ngo->org_id}}">View</a></td>
                                            <td><a href="../orgAdmin/index/{{$ngo->org_id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                            <td><input type="checkbox" value="{{$ngo->org_id}}" name="featured[]" <?php if($ngo->flag=='1') echo "checked"; ?>></td>
                                        </tr>
                                    @endforeach
                                        <div style="text-align: right"><button class="btn btn-success" type="submit">Make selected featured</button><p class="help-block">(First 10 NGOs selected will onl be considered for featured tag)</p></div>
                                </form>
                            </table>

                        </div>
                        <div class="panel-footer"><span style="alignment: center"><?php echo $ngoList->render();?></span></div>
                    </div>
                @endif
                @if(\Session::get('contentType')=='topNGO')
                        <div class="table-responsiveness">
                            <div class="panel panel-info">
                                <div class="panel-heading"><h3 class="panel-title" style="text-align:center">Top Listed NGOs</h3></div>
                                    <div class="panel-body">
                                        <table class="table table-hover table-bordered">
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Grade</th>
                                                <th>Edit</th>
                                                <th>View Microwebsite</th>
                                            </tr>
                                            @foreach($topNGO as $ngo)
                                            <tr>
                                                <td>{{$ngo->org_id}}</td>
                                                <td>{{$ngo->name}}</td>
                                                <td>{{$ngo->grades}}</td>
                                                <td><a href="../orgAdmin/index/{{$ngo->org_id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                                <td><a href="../microwebsite/{{$ngo->org_id}}">View</a></td>
                                            </tr>
                                            @endforeach
                                            <div style="text-align: right">
                                                <form action="generateTopNGOs" method="POST">
                                                    <div class="form-group">
                                                        <button class="btn btn-warning" name="generateTopNGOs" type="submit">Refresh this list</button>
                                                        <p class="help-block">(This will override all previously listed top NGOs)</p>
                                                    </div>
                                                </form>
                                            </div>
                                        </table>
                                    </div>
                                <div class="panel-footer"><span style="alignment: center"><?php echo $topNGO->render();?></span></div>
                            </div>
                        </div>
                @endif

                @if(\Session::get('contentType')=='featuredNGO')
                        <div class="table-responsiveness">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title" style="text-align: center;">Featured NGOs</h3>
                                </div>
                                <div class="panel-body">
                                    <form action="removeFeatured" method="POST">
                                    <table class="table table-hover table-bordered" style="text-align: center">
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>View Microwebsite</th>
                                            <th>Edit</th>
                                            <th>Remove featured</th>
                                        </tr>
                                        @foreach($featured as $fea)
                                        <tr>
                                            <td>{{$fea->org_id}}</td>
                                            <td>{{$fea->name}}</td>
                                            <td><a href="../microwebsite/{{$fea->org_id}}">View</a></td>
                                            <td><a href="../orgAdmin/index/{{$fea->org_id}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                                            <td><input type="checkbox" value="{{$fea->org_id}}" name="unfeature[]" <?php if($fea->flag=='1') echo "unchecked"; ?>></td>
                                        </tr>
                                        @endforeach
                                        <div style="text-align: right">
                                            <button class="btn btn-danger" name="removeFeatured" type="submit">Unfeature selected NGOs</button>
                                            <p class="help-block">(Changes once made here, cannot be reversed)</p>
                                        </div>
                                    </table>
                                    </form>
                                </div>
                                <div class="panel-footer"><span style="alignment: center"><?php echo $featured->render();?></span></div>
                            </div>
                        </div>
                    @endif
            </div>
        </div>
    </div>

    <div class="container">
            <div class="col-md-6 col-sm-12">

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
    </div>

@endsection
