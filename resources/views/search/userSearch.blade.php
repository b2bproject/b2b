<?php
/**
 * Created by PhpStorm.
 * User: thada
 * Date: 24-03-2016
 * Time: 19:21
 */
?>
@extends('user.header')
@section('content')
<div class="container">
    <div class="row">
        <h1 class="page-header heading">Search Results</h1>
    </div>
    @include('partials.flash')
    @include('errors.lists')
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-offset-8 col-md-4 col-sm-4 col-xs-12">
            <form action="search" role="form" method="get">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search by name, location, product" name="search" id="search">
						<span class="input-group-btn">
							<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
						</span>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(!(\Session::has('searchOrg') && \Session::has('searchLocality') && \Session::has('searchProduct')))
                <h1 class="heading" style="text-align: center">No results found</h1>
            @else
                @if(\Session::has('searchOrg'))
                    <h3 class="page-header">NGO's matching your search</h3>
                    <?php $organisation = \Session::get('searchOrg'); ?>
                    <div class="table-responsive">
                        <table class="table">
                            @foreach($organisation as $org)
                                <tr>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                        <img src="public/uploads/{{$org->logo}}" alt="Logo" class="img-responsive">
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                                        <h2><a href="microwebsite/{{$org->org_id}}" target="_blank">{{$org->name}}</a></h2>
                                                        <p>{{$org->description}}</p>
                                                        <a href="http://{{$org->website}}" target="_blank">{{$org->website}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif

                @if(\Session::has('searchLocality'))
                    <h3 class="page-header">NGO's based on locality</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <?php $locality = \Session::get('searchLocality');?>
                            @foreach($locality as $loc)
                                <tr>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-2 col-sm-2 col-xs-2">
                                                        <img src="public/uploads/{{$loc->logo}}" alt="Logo" class="img-responsive">
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                                        <a href="microwebsite/{{$loc->org_id}}" target="_blank"><h2>{{$loc->name}}</h2></a>
                                                        <p>{{$loc->description}}</p>
                                                        <p>{{$loc->locality}}</p>
                                                        <a href="http://{{$loc->website}}" target="_blank">{{$loc->website}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif

                    @if(\Session::has('searchProduct'))
                        <h3 class="page-header">Products based on your search</h3>
                        <div class="table-responsive">
                            <table class="table">
                                <?php $product = \Session::get('searchProduct'); ?>
                                @foreach($product as $prod)
                                    <tr>
                                        <td>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-2 col-sm-2 col-xs-2">
                                                            <img src="public/uploads/{{$prod->image}}" alt="Logo" class="img-responsive">
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                                            <h2><a href="microwebsite/{{$prod->org_id}}" target="_blank">{{$prod->product_name}}</a></h2>
                                                            <p>{{$prod->description}}</p>
                                                            <p>{{$prod->cost}}</p>
                                                              <?php
                                                              $ngo = \DB::table('organisation')->select('name')->where('org_id','=',$prod->org_id)->pluck('name');
                                                                      ?>
                                                            <p>NGO: {{$ngo}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif


            @endif
        </div>
    </div>
</div>
@endsection
