<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>B2B</title>
    <link href="http://localhost/b2b/public/asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://localhost/b2b/public/asset/css/bootstrap-theme.min.css" rel="stylesheet" >
    <link href="http://localhost/b2b/public/asset/css/style.css" rel="stylesheet" >
</head>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid" id="header">


        <!-- div class="navbar " id="my-navbar"-->

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="padding: 12px 2px 0px 15px;">
                <img src="images/logo3.png" class="img-responsive" /></a>
            <p class="navbar-text" style="color:white; font-size:25px; padding:12px 0px 0px 0px;">B2B Management</p>
        </div><!-- End navbar-header-->
        <div class="collapse navbar-collapse" id="navbar-collapse">

            <ul class="nav navbar-nav navbar-right "> <!--style="padding:20px 0px 0px 0px;"-->
            <li><a href="#">HOME</a></li>
            <li><a href="#">CONTACT US</a></li>
            <li><a href="#">ORGANISATIONS</a></li>
            <li><a href="#">ADD ORGANISATIONS</a></li>
            <a href="#" class="btn navbar-btn  btn-primary">Login/ Register</a>
            </ul>
        </div>
        <!-- End container-->
    </div><!-- End navbar-->

</nav>

<body style="font-family: Georgia; padding-top: 70px">

@yield('content')

@yield('footer')
<script src="http://localhost/b2b/public/asset/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="http://localhost/b2b/public/asset/js/bootstrap.min.js"></script>
</body>
</html>