<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 01-03-2016
 * Time: 09:59
 */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>B2B</title>
    <link href="http://localhost/b2b/public/asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://localhost/b2b/public/asset/css/bootstrap-theme.min.css" rel="stylesheet" >
    <link href="http://localhost/b2b/public/asset/css/style.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>
    <script>
        $(function() {
            $( "#datepicker2" ).datepicker();
        });
    </script>
</head>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid" id="header">

        <!-- div class="navbar " id="my-navbar"-->

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="true">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="padding: 12px 2px 0px 15px;">
                <img src="http://localhost/b2b/public/asset/images/logo3.png" class="img-responsive" /></a>
            <p class="navbar-text" style="color:white; font-size:25px; padding:12px 0px 0px 0px;">B2B Management</p>
        </div><!-- End navbar-header-->
        <div class="collapse navbar-collapse" id="navbar-collapse">

            <ul class="nav navbar-nav navbar-right " style="padding:20px 0px 0px 0px;">
                <li><a href="http://localhost/b2b/public/orgAdmin/index">Home</a></li>
                <li><a href="#">View Statistics</a></li>
                <li><a href="http://localhost/b2b/public/contactAdmin">Contact Site Admin</a></li>
                @if(Session::get('userStatus')==2)
                    <li><a href="user/addNGO">Add Organisation</a></li>
                @endif
                @if(Session::has('userSession'))
                    <li><a href="user/logout">Logout</a></li>
                @endif
                <li><a href="http://localhost/b2b/public/microwebsite/@if(\Session::has('orgID')){{\Session::get('orgID')}}@else{{\Session::get('adminOrgID')}}@endif">Preview micro-website</a></li>
                <li><a href="http://localhost/b2b/public/orgAdmin/editngo">Edit NGO Details</a></li>
            </ul>
        </div>
        <!-- End container-->
    </div><!-- End navbar-->

</nav>

<body style="font-family: Georgia; padding-top: 90px">

@yield('content')

@yield('footer')

<footer style="background-color: #8ba3c2; color: #fff5fd; min-height: 25px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-link" href="#" role="button" style="color: #fff">About Us</a>
                <a class="btn btn-link" href="#" role="button" style="color: #fff">Terms & Conditions</a>
                <a class="btn btn-link" href="#" role="button" style="color: #fff">Contact Site Admin</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="col-md-offset-3 col-md-9">
                <h5 style="color: #fff; float: right">Copyrights &copy; 2014 reseved by B2B Management and Lead Generation</h5>
            </div>
            </div>
        </div>
    </div>
</footer>
<script src="http://localhost/b2b/public/asset/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="http://localhost/b2b/public/asset/js/bootstrap.min.js"></script>
</body>
</html>
