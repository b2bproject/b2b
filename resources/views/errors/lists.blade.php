<?php
/**
 * Created by PhpStorm.
 * User: Nilesh Thadani
 * Date: 12-02-2016
 * Time: 09:40
 */?>
@if($errors->any())
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif