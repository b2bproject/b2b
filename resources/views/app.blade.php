<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My First Website</title>
    <link href="http://localhost/b2b/public/asset/css/bootstrap.css" rel="stylesheet">
    <link href="http://localhost/b2b/public/asset/css/bootstrap-theme.min.css" rel="stylesheet" >
    <!--<link href="http://localhost/b2b/public/asset/css/style.css" rel="stylesheet" >-->
    <style type="text/css">
        #header{
            background-image: url("http://localhost/b2b/public/asset/images/textured.jpg");
        }
        .nav.navbar-nav li a{
            color: #ffffff;
            font-weight: bold;
            font-size: 17px;
        }
    </style>

</head>
<body style="font-family: Georgia; ">

@yield('content')

@yield('footer')
<script src="http://localhost/b2b/public/asset/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="http://localhost/b2b/public/asset/js/bootstrap.min.js"></script>
</body>
</html>