@extends('user.header')

<style>
    .req{
        color: #C20000;
    }
</style>

@section('content')

    <div class="container">
        <h1 class="heading page-header"><span style="font-size: 140%; color: #3f45ad;">Add Details&nbsp;</span><small style="font-size: 15px">Fields marked <span class="req">red&nbsp;</span>are mandatory</small></h1>
        @include('partials/flash')
        @include('errors.lists')
        @foreach($organ as $org)
            <form action="http://localhost/b2b/public/ngo/{{$org->org_id}}" class="form-horizontal" method="post">
                <div class="container">
                    <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="form-group">
                                    <label for="name" class="col-md-2 control-label req">Name</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="text" id="name" placeholder="Your name" required class="form-control" value="@if(isset($org->name)){{$org->name}}@endif" name="name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="license" class="col-md-2 control-label req">License No.</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="text" id="license" placeholder="Your license no." class="form-control" value="@if(isset($org->license_no)){{$org->license_no}} @endif" name="license_no" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="president" class="col-md-2 control-label req">President Name</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="text" id="president" placeholder="Your president name" class="form-control" required name="president_name" value="@if(isset($org->president_name)){{$org->president_name}}@endif">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pri_email" class="col-md-2 control-label req">Primary Email</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="email" id="pri_email" placeholder="Your primary email" value="@if(isset($org->primary_email)){{$org->primary_email}}@endif" class="form-control" required name="primary_email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="sec_email" class="col-md-2 control-label">Secondary Email</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="email" id="sec_email" placeholder="Your secondary email" class="form-control" name="secondary_email" value="@if(isset($org->secondary_email)){{$org->secondary_email}}@endif">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="website" class="col-md-2 control-label">Website</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="text" id="website" placeholder="Your website" class="form-control" name="website" value="@if(isset($org->website)){{$org->website}}@endif">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="contact no" class="col-md-2 control-label req">Contact No.</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="text" name="contact_no" id="contact no" placeholder="Your contact no." class="form-control" value="@if(isset($org->contact_no)){{$org->contact_no}}@endif" required>
                                    </div>
                                </div>

                                <input type="hidden" name="status" value="1">


                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="form-group">
                                    <label for="description" class="col-md-2 control-label req">Description</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <textarea type="text" name="description" id="description" placeholder="Provide some description of your organisation giving some insights" class="form-control" rows="2" cols="20">@if(isset($org->description)){{$org->description}}@endif</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-md-2 control-label req">Address</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <textarea type="text" name="address" id="address" placeholder="Your address" class="form-control" rows="1" cols="20">@if(isset($org->address)){{$org->address}}@endif</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="locality" class="col-md-2 control-label req">Locality</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="text" id="locality" name="locality" value="@if(isset($org->locality)){{$org->locality}}@endif"placeholder="Your loaclity" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="city" class="col-md-2 control-label req">City</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <input type="text" name="city" id="city" value="@if(isset($org->city)){{$org->city}}@endif" placeholder="Your city" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-md-2 control-label req">State</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <?php $c = $org->state_id;
                                        //echo $c;?>
                                        <?php  $stat = \DB::table('state')->where('state_id', $c)->pluck('name'); ?>
                                        <select id="state" name="state_id" class="form-control">
                                            <option selected value="{{$org->state_id}}">{{$stat}}</option>
                                            <option disabled>Please select a state</option>
                                            @foreach ($states as $state => $name)
                                                <option value="{{$state}}">{{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="country" class="col-md-2 control-label req">Country</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">

                                       <?php $c = $org->country_id;
                                        //echo $c;?>
                                         <?php  $cont = \DB::table('country')->where('country_id', $c)->pluck('name'); ?>
                                        <select id="country" name="country_id" class="form-control">

                                            <option selected value="{{$org->country_id}}">{{$cont}}</option>

                                            @foreach ($countries as $country => $name)
                                                <option value="{{$country}}">{{$name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="type_of_work_id" class="col-md-2 control-label req">Type of Work</label>
                                    <div class="col-md-10 col-sm-10 col-xs-10">
                                        <?php $c = $org->type_of_work_id;
                                        ?>
                                        <?php  $cont = \DB::table('type_of_work')->where('type_of_work_id', $c)->pluck('type'); ?>
                                        <select name="type_of_work_id" id="type_of_work_id" class="form-control">
                                            <option selected value="{{$org->type_of_work_id}}">{{$cont}}</option>
                                            <option disabled>Please select the type of work</option>
                                            @foreach ($works as $work => $type)
                                                <option value="{{$work}}">{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                            </div>

                    </div>


                    <div class="row">
                        <div class="form-group">
                            <div class="col-lg-offset-2">
                                <div class=" col-lg-4 col-md-6 col-sm-12 col-xs-12" style="margin: 5px">
                                    <button type="submit" class="btn btn-primary btn-block">Save your NGO</button>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12" style="margin: 5px">
                                    <button type="reset" class="btn btn-default btn-block">Reset form and fill again</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        @endforeach

    </div>


@endsection